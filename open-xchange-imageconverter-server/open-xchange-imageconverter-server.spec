Name:          open-xchange-imageconverter-server
BuildArch:     noarch
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: ant
BuildRequires: open-xchange-core >= @OXVERSION@
BuildRequires: open-xchange-admin-soap >= @OXVERSION@
BuildRequires: open-xchange-authorization-standard >= @OXVERSION@
BuildRequires: open-xchange-file-distribution >= @OXVERSION@
BuildRequires: open-xchange-filestore-s3 >= @OXVERSION@
BuildRequires: open-xchange-filestore-sproxyd >= @OXVERSION@
BuildRequires: open-xchange-grizzly >= @OXVERSION@
BuildRequires: open-xchange-oauth >= @OXVERSION@
BuildRequires: open-xchange-oauth-provider >= @OXVERSION@
BuildRequires: open-xchange-rest >= @OXVERSION@
BuildRequires: open-xchange-sessionstorage-hazelcast >= @OXVERSION@
Version:       @OXVERSION@
%define        ox_release 3
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       AGPLv3+
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Source2:       open-xchange-imageconverter-server.service
%define        dropin_dir /etc/systemd/system/open-xchange-imageconverter-server.service.d
%define        dropin_example limits.conf
Summary:       The Open-Xchange webservice for image conversion
AutoReqProv:   no
Requires:      gawk
Requires:      curl
Requires:      sed
Requires:      open-xchange-core >= @OXVERSION@
Requires:      open-xchange-admin-soap >= @OXVERSION@
Requires:      open-xchange-file-distribution >= @OXVERSION@
Requires:      open-xchange-filestore-s3 >= @OXVERSION@
Requires:      open-xchange-filestore-sproxyd >= @OXVERSION@
Requires:      open-xchange-grizzly >= @OXVERSION@
Requires:      open-xchange-rest >= @OXVERSION@
Provides:      open-xchange-imageconverter-server = %{version}
Requires(pre):    systemd
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

%description
This package contains the backend components for the ImageConverter web service

Authors:
--------
    Open-Xchange

%prep
%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build
mkdir -p %{buildroot}/var/log/open-xchange/imageconverter
mkdir -p %{buildroot}/var/spool/open-xchange/fileitem
mkdir -p %{buildroot}/var/spool/open-xchange/imageconverter
%__install -D -m 444 %{SOURCE2} %{buildroot}/usr/lib/systemd/system/open-xchange-imageconverter-server.service

# On Redhat and SuSE start scripts are not automatically added to system start. This is wanted behavior and standard.

%post
if [ ! -f %{dropin_dir}/%{dropin_example} ]
then
  install -D -m 644 %{_defaultdocdir}/%{name}-%{version}/%{dropin_example} %{dropin_dir}/%{dropin_example}
fi

drop_in=%{dropin_dir}/%{dropin_example}
if [ -f ${drop_in} ] && grep -q "^#LimitNOFILE=16384$" ${drop_in}
then
  sed -i 's/^#LimitNOFILE=16384$/#LimitNOFILE=65536/' ${drop_in}
fi

drop_in=%{dropin_dir}/%{dropin_example}
if [ -f ${drop_in} ] && ! grep -q LimitNPROC ${drop_in}
then
  sed -i '/^\[Service\]$/a #LimitNPROC=65536' ${drop_in}
fi

# Trigger a service definition/config reload
systemctl daemon-reload &> /dev/null || :

if [ ${1:-0} -eq 2 ]; then
    # only when updating

    . /opt/open-xchange/lib/oxfunctions.sh

    # prevent bash from expanding, see bug 13316
    GLOBIGNORE='*'

    SCR=SCR-476
    ox_scr_todo ${SCR} && {
      pfile=/opt/open-xchange/imageconverter/etc/overwrite.properties
      pkey=com.openexchange.hazelcast.enabled
      new_val=false
      ox_add_property ${pkey} "${new_val}" ${pfile}
      ox_scr_done ${SCR}
    }

    if ox_scr_todo SCR-486
    then
      pfile=/opt/open-xchange/imageconverter/etc/fileitem.properties
      keys=( com.openexchange.fileitem.readProperty.15 com.openexchange.fileitem.readProperty.16 com.openexchange.fileitem.readProperty.17 com.openexchange.fileitem.readProperty.18 com.openexchange.fileitem.readProperty.19 com.openexchange.fileitem.readProperty.20 com.openexchange.fileitem.readProperty.21 com.openexchange.fileitem.readProperty.22 com.openexchange.fileitem.readProperty.23 com.openexchange.fileitem.writeProperty.15 com.openexchange.fileitem.writeProperty.16 com.openexchange.fileitem.writeProperty.17 com.openexchange.fileitem.writeProperty.18 com.openexchange.fileitem.writeProperty.19 com.openexchange.fileitem.writeProperty.20 com.openexchange.fileitem.writeProperty.21 com.openexchange.fileitem.writeProperty.22 com.openexchange.fileitem.writeProperty.23 )
      values=( "requireSSL=false" "verifyServerCertificate=false" "enabledTLSProtocols=false" "clientCertificateKeyStoreUrl=" "clientCertificateKeyStorePassword=" "clientCertificateKeyStoreType=" "trustCertificateKeyStoreUrl=" "trustCertificateKeyStorePassword=" "trustCertificateKeyStoreType=" "requireSSL=false" "verifyServerCertificate=false" "enabledTLSProtocols=false" "clientCertificateKeyStoreUrl=" "clientCertificateKeyStorePassword=" "clientCertificateKeyStoreType=" "trustCertificateKeyStoreUrl=" "trustCertificateKeyStorePassword=" "trustCertificateKeyStoreType=" )
      for I in $(seq 0 $(expr ${#keys[@]} - 1))
      do
        ox_add_property ${keys[$I]} "${values[$I]}" ${pfile}
      done
      ox_scr_done SCR-486
    fi

    if ox_scr_todo SCR-732
    then
      pfile=/opt/open-xchange/imageconverter/etc/fileitem.properties
      pkey_read=com.openexchange.fileitem.readProperty.17
      pkey_write=com.openexchange.fileitem.writeProperty.17
      old_val=enabledTLSProtocols=false
      new_val=enabledTLSProtocols=TLSv1,TLSv1.1,TLSv1.2
      value=$(ox_read_property ${pkey_read} ${pfile})
      if [ "${old_val}" = "${value}" ]
      then
        ox_set_property ${pkey_read} ${new_val} ${pfile}
      fi
      value=$(ox_read_property ${pkey_write} ${pfile})
      if [ "${old_val}" = "${value}" ]
      then
        ox_set_property ${pkey_write} ${new_val} ${pfile}
      fi
      ox_scr_done SCR-732
    fi

fi

%preun

%postun

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /opt/open-xchange/imageconverter/bundles/
/opt/open-xchange/imageconverter/bundles/*
%dir %attr(750, open-xchange, root) /opt/open-xchange/imageconverter/osgi/
/opt/open-xchange/imageconverter/osgi/config.ini.template
%dir /opt/open-xchange/imageconverter/osgi/bundle.d/
/opt/open-xchange/imageconverter/osgi/bundle.d/*
%dir /opt/open-xchange/imageconverter/etc/
%config(noreplace) /opt/open-xchange/imageconverter/etc/fileitem.properties
%config(noreplace) /opt/open-xchange/imageconverter/etc/objectcache.properties
%config(noreplace) /opt/open-xchange/imageconverter/etc/objectcache-s3.properties
%config(noreplace) /opt/open-xchange/imageconverter/etc/overwrite.properties
%config(noreplace) /opt/open-xchange/imageconverter/etc/process-conf.sh
%dir /opt/open-xchange/imageconverter/etc/security
/opt/open-xchange/imageconverter/etc/security/imageconverter.list
%dir /opt/open-xchange/imageconverter/etc/magick
/opt/open-xchange/imageconverter/etc/magick/policy.xml
%dir %attr(755, open-xchange, root) /opt/open-xchange/imageconverter/bin/
%attr(0755,open-xchange,root) /opt/open-xchange/imageconverter/bin/imageconverter-kill-orphans
%dir %attr(750, open-xchange, root) /opt/open-xchange/imageconverter/lib/
%attr(0755,root,root) /opt/open-xchange/imageconverter/lib/com.openexchange.imageconverter.clt.jar
%dir %attr(750, open-xchange, root) /opt/open-xchange/imageconverter/sbin/
%attr(0755,root,root) /opt/open-xchange/imageconverter/sbin/imageconverter-admin
%attr(0755,root,root) /opt/open-xchange/imageconverter/sbin/initfileitemdb
%attr(0755,root,root) /opt/open-xchange/imageconverter/sbin/open-xchange-imageconverter
%dir %attr(750, open-xchange, root) /var/log/open-xchange/imageconverter
%dir %attr(750, open-xchange, root) /var/spool/open-xchange/fileitem
%dir %attr(750, open-xchange, root) /var/spool/open-xchange/imageconverter
/usr/lib/systemd/system/open-xchange-imageconverter-server.service
%doc docs/%{dropin_example}

%changelog
* Thu Nov 28 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate of 7.10.3 release
* Thu Nov 21 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate of 7.10.3 release
* Thu Oct 17 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.3 release
* Wed Jun 19 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.3 release
* Fri May 10 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate of 7.10.2 release
* Thu May 02 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.10.2 release
* Thu Mar 28 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.2 release
* Mon Mar 11 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.2
* Fri Nov 23 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
RC 1 for 7.10.1 release
* Fri Nov 02 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview for 7.10.1 release
* Thu Oct 11 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview for 7.10.1 release
* Mon Sep 10 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.1
* Mon Jun 25 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.10.0 release
* Mon Jun 11 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.10.0 release
* Fri May 18 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth preview of 7.10.0 release
* Thu Apr 19 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth preview of 7.10.0 release
* Tue Apr 03 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Third preview of 7.10.0 release
* Tue Feb 20 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.10.0 release
* Fri Feb 02 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.0 release
* Mon Oct 23 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.0 release
* Tue Apr 04 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.8.4 release
