#! /bin/bash

set -e

PATH_TO_NODE=/usr/bin/node
PATH_TO_CODEGEN=./codegen
CODEGEN_SCRIPT_NAME=codegen.sh
CLIENT_GENERATED_TARGET_DIR=../com.openexchange.imageconverter.client/src/com/openexchange/imageconverter/client/generated

# combine swagger spec fragments into one swagger.json
# file to be used by subsequent tools
${PATH_TO_NODE} resolve.js ./imageconverter

# call shell script to generate appropriate client/server
# code (currently client generated code only)
pushd "${PATH_TO_CODEGEN}"
rm -rvf generated/*
./"${CODEGEN_SCRIPT_NAME}"
popd

# clear old content in target dir
rm -rvf "${CLIENT_GENERATED_TARGET_DIR}"/*

# copy generated client resources to client target dir
rsync -Sauv "${PATH_TO_CODEGEN}/generated/src/main/java/com/openexchange/imageconverter/client/generated/" "${CLIENT_GENERATED_TARGET_DIR}/"
rm -rvf "${PATH_TO_CODEGEN}/generated/"*




