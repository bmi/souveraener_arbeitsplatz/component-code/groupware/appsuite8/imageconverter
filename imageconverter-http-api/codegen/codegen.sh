#! /bin/sh

PATH_TO_SWAGGER_CODEGEN_CLI=/usr/local/bin/swagger-codegen

"${PATH_TO_SWAGGER_CODEGEN_CLI}" generate -c ./config -t ./templates/java/ -i ../imageconverter/swagger.json -o ./generated/ -l java
