#Installation guide for local setup

##Setup perl libraries

To run the ImageConverter munin scripts you have to install the following packages

```
sudo apt-get install libwww-perl
sudo apt-get install libjson-perl
sudo apt-get install open-xchange-munin-scripts*
sudo apt-get install open-xchange-munin-scripts-jolokia*

```
##Set environment variables

You also have to set environment variables in order to properly configure the open-xchange-munin monitoring system. These need to be consistent to "jolokia.properties"

```
oxJolokiaUrl=http://localhost:8009/monitoring/jolokia
export oxJolokiaUrl

oxJolokiaUser=admin
export oxJolokiaUser

oxJolokiaPassword=secret
export oxJolokiaPassword
```

Now you should be able to run the ImageConverter munin scripts