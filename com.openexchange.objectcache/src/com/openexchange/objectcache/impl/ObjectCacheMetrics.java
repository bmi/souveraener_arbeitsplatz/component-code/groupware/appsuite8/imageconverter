/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import com.openexchange.metrics.micrometer.Micrometer;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.objectcache.api.ObjectCacheException;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

/**
 * {@link ObjectCacheMetrics}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class ObjectCacheMetrics {

    final private static char CHAR_DOT = '.';

    final private static String STR_ADDOBJECTS = "addobjects";
    final private static String STR_ADMIN = "admin";
    final private static String STR_ALL = "all";
    final private static String STR_BYTES = "bytes";
    final private static String STR_CACHED_FOR_A_GROUP = "cached for a group.";
    final private static String STR_COUNT = "count";
    final private static String STR_CURRENT = "current";
    final private static String STR_MEDIANDURATION = "medianduration";
    final private static String STR_GETGROUPINFO = "getgroupinfo";
    final private static String STR_GETOBJECTS = "getobjects";
    final private static String STR_GROUP = "group";
    final private static String STR_HASOBJECTS = "hasobjects";
    final private static String STR_MAXIMUM = "maximum";
    final private static String STR_ITEMS = "items";
    final private static String STR_MODIFICATION = "modification";
    final private static String STR_MS = "ms";
    final private static String STR_NO_UNIT = null;
    final private static String STR_OBJECTCACHE = "objectcache";
    final private static String STR_OLDEST = "oldest";
    final private static String STR_REGISTERGROUP = "registergroup";
    final private static String STR_REQUEST = "request";
    final private static String STR_REQUESTS_FOR_A_GROUP = "requests for a group.";
    final private static String STR_SIZE = "size";
    final private static String STR_STATUS = "status";
    final private static String STR_TOTAL = "total";

    final private static String NAME_REQUEST_COUNT = STR_OBJECTCACHE + CHAR_DOT + STR_REQUEST + CHAR_DOT + STR_COUNT;
    final private static String NAME_REQUEST_DURATION = STR_OBJECTCACHE + CHAR_DOT + STR_REQUEST + CHAR_DOT + STR_MEDIANDURATION + CHAR_DOT + STR_MS;

    final private static String NAME_CACHE_COUNT = STR_OBJECTCACHE + CHAR_DOT + STR_ITEMS + CHAR_DOT;
    final private static String NAME_CACHE_MAXCOUNT = NAME_CACHE_COUNT + STR_MAXIMUM + CHAR_DOT + STR_COUNT;
    final private static String NAME_CACHE_CURCOUNT = NAME_CACHE_COUNT + STR_CURRENT + CHAR_DOT + STR_COUNT;

    final private static String NAME_CACHE_SIZE = STR_OBJECTCACHE + CHAR_DOT + STR_SIZE + CHAR_DOT;
    final private static String NAME_CACHE_MAXSIZE = NAME_CACHE_SIZE + STR_MAXIMUM;
    final private static String NAME_CACHE_CURSIZE = NAME_CACHE_SIZE + STR_CURRENT;

    final private static String NAME_CACHE_OLDEST_MODIFICATION = STR_OBJECTCACHE + CHAR_DOT + STR_OLDEST + CHAR_DOT + STR_MODIFICATION + CHAR_DOT + STR_MS;

    final private static Tags TAG_REQUEST_COUNT_STATUS = Tags.of(STR_REQUEST, STR_STATUS);
    final private static Tags TAG_REQUEST_COUNT_REGISTERGROUP = Tags.of(STR_REQUEST, STR_REGISTERGROUP);
    final private static Tags TAG_REQUEST_COUNT_GETGROUPINFO = Tags.of(STR_REQUEST, STR_GETGROUPINFO);
    final private static Tags TAG_REQUEST_COUNT_HASOBJECTS = Tags.of(STR_REQUEST, STR_HASOBJECTS);
    final private static Tags TAG_REQUEST_COUNT_GETOBJECTS = Tags.of(STR_REQUEST, STR_GETOBJECTS);
    final private static Tags TAG_REQUEST_COUNT_ADDOBJECTS = Tags.of(STR_REQUEST, STR_ADDOBJECTS);

    final private static Tags TAG_REQUEST_DURATION_ALL = Tags.of(STR_REQUEST, STR_ALL);
    final private static Tags TAG_REQUEST_DURATION_STATUS = Tags.of(STR_REQUEST, STR_STATUS);
    final private static Tags TAG_REQUEST_DURATION_REGISTERGROUP = Tags.of(STR_REQUEST, STR_REGISTERGROUP);
    final private static Tags TAG_REQUEST_DURATION_GETGROUPINFO = Tags.of(STR_REQUEST, STR_GETGROUPINFO);
    final private static Tags TAG_REQUEST_DURATION_HASOBJECTS = Tags.of(STR_REQUEST, STR_HASOBJECTS);
    final private static Tags TAG_REQUEST_DURATION_GETOBJECTS = Tags.of(STR_REQUEST, STR_GETOBJECTS);
    final private static Tags TAG_REQUEST_DURATION_ADDOBJECTS = Tags.of(STR_REQUEST, STR_ADDOBJECTS);

    // use an odd number for request duration count to speed up finding of the median value
    final private static int REQUEST_DURATION_COUNT = 511;

    /**
     * {@link RequestType}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public enum RequestType {
        TOTAL,
        STATUS,
        REGISTERGROUP,
        GETGROUPINFO,
        HASOBJECTS,
        GETOBJECTS,
        ADDOBJECTS
    }

    /**
     * Initializes a new {@link ObjectCacheMetrics}.
     * @param objectCache
     */
    public ObjectCacheMetrics(@NonNull final IObjectCache objectCache) {
        super();

        m_objectCache = objectCache;

        // create request count map for Admin group
        final Map<RequestType, AtomicLong> adminGroupRequestCountMap = new HashMap<>();

        for (final RequestType curRequestType : RequestType.values()) {
            adminGroupRequestCountMap.put(curRequestType, new AtomicLong(0));
            m_requestDurationMap.put(curRequestType, new LinkedList<>());
        }

        m_groupRequestCountMap.put(STR_ADMIN, adminGroupRequestCountMap);

        // initialize static metrics
        implInit();
    }

    /**
     * @param requestType
     * @param requestDurationMillis
     */
    public void incrementAdminRequestCount(@NonNull final RequestType requestType, final long requestDurationMillis) {
        incrementRequestCount(STR_ADMIN, requestType, requestDurationMillis);
    }

    /**
     * @param group
     * @param requestType
     * @param requestDurationMillis
     */
    public void  incrementRequestCount(
        @NonNull final String group,
        @NonNull final RequestType requestType,
        final long requestDurationMillis) {

        final Map<RequestType, AtomicLong> groupRequestCountMap = m_groupRequestCountMap.get(group);

        if (null != groupRequestCountMap) {
            groupRequestCountMap.get(requestType).incrementAndGet();
        }

        final LinkedList<Long> requestDurationList = m_requestDurationMap.get(requestType);

        // add value to request type list
        synchronized (requestDurationList) {
            if (requestDurationList.size() == REQUEST_DURATION_COUNT) {
                // ensure a maximum number of duration entries
                requestDurationList.removeFirst();
            }

            requestDurationList.addLast(Long.valueOf(requestDurationMillis));
        }

        // add value to all requests list
        final LinkedList<Long> requestDurationAllList = m_requestDurationAllList;

        synchronized (requestDurationAllList) {
            if (requestDurationAllList.size() == REQUEST_DURATION_COUNT) {
                // ensure a maximum number of duration entries
                requestDurationAllList.removeFirst();
            }

            requestDurationAllList.addLast(Long.valueOf(requestDurationMillis));
        }
    }

    /**
     * @param requestType
     * @return
     */
    public long getAdminRequestCount(@NonNull final RequestType requestType) {
        return getRequestCount(STR_ADMIN, requestType);
    }

    /**
     * @param group
     * @param requestType
     * @return
     */
    public long getRequestCount(
        @NonNull final String group,
        @NonNull final RequestType requestType) {

        final Map<RequestType, AtomicLong> groupRequestCountMap = m_groupRequestCountMap.get(group);

        if (null != groupRequestCountMap) {
            if (RequestType.TOTAL == requestType) {
                // return sum of all available request types
                long requestCountAll = 0;

                for (final RequestType curRequestType : RequestType.values()) {
                    // AtomicLong value => no outer synchronization needed
                    requestCountAll += groupRequestCountMap.get(curRequestType).get();
                }

                return requestCountAll;
            }

            // return count for given request type (AtomicLong value => no outer synchronization needed)
            return groupRequestCountMap.get(requestType).get();
        }

        return 0;
    }

    /**
     * @param requestType
     * @return
     */
    public long getMedianRequestDurationMillis(@Nullable final RequestType requestType) {
        if (RequestType.TOTAL == requestType) {
            final LinkedList<Long> requestDurationAllList = m_requestDurationAllList;

            synchronized (requestDurationAllList) {
                return implGetMedianValue(requestDurationAllList);
            }
        }

        // get median request duration for given request type
        final LinkedList<Long> requestDurationList = m_requestDurationMap.get(requestType);

        synchronized (requestDurationList) {
            return implGetMedianValue(requestDurationList);
        }
    }

    /**
     *
     */
    public void shutdown() {
        m_running.compareAndSet(true, false);
    }

    /**
     * @return
     */
    public boolean isRunning() {
        return m_running.get();
    }

    // - Implementation --------------------------------------------------------

    /**
     * registering the metrics, available in every case
     */
    private void implInit() {
        final Tags adminGroupTags = Tags.of(STR_GROUP, STR_ADMIN);
        final String requestsForAdmin = STR_REQUESTS_FOR_A_GROUP + " '" + STR_ADMIN +"'.";


        // Request counts for "Admin" group
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_COUNT_STATUS.and(adminGroupTags), "The number of health and status related " + requestsForAdmin, STR_NO_UNIT, this,
            (m) -> (isRunning() ? getRequestCount(STR_ADMIN, RequestType.STATUS) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT + CHAR_DOT + STR_TOTAL, adminGroupTags, "The total number of " + requestsForAdmin, STR_NO_UNIT, this,
            (m) -> (isRunning() ? getRequestCount(STR_ADMIN, RequestType.TOTAL) : 0.0));

        // Median request processing times
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_DURATION_ALL, "The median processing time of requests in milliseconds.", STR_NO_UNIT, this,
            (m) -> (isRunning() ? getMedianRequestDurationMillis(RequestType.TOTAL) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_DURATION_STATUS, "The median processing time of health and status related requests.", STR_MS, this,
            (m) -> (isRunning() ? getMedianRequestDurationMillis(RequestType.STATUS) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_DURATION_REGISTERGROUP, "The median processing time of registerGroup requests.", STR_MS, this,
            (m) -> (isRunning() ? getMedianRequestDurationMillis(RequestType.REGISTERGROUP) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_DURATION_GETGROUPINFO, "The median processing time of getGroupInfo requests.", STR_MS, this,
            (m) -> (isRunning() ? getMedianRequestDurationMillis(RequestType.GETGROUPINFO) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_DURATION_HASOBJECTS, "The median processing time of hasObjects requests.", STR_MS, this,
            (m) -> (isRunning() ? getMedianRequestDurationMillis(RequestType.HASOBJECTS) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_DURATION_GETOBJECTS, "The median processing time of getObjects requests.", STR_MS, this,
            (m) -> (isRunning() ? getMedianRequestDurationMillis(RequestType.GETOBJECTS) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_DURATION_ADDOBJECTS, "The median processing time of addObjects requests.", STR_MS, this,
            (m) -> (isRunning() ? getMedianRequestDurationMillis(RequestType.ADDOBJECTS) : 0.0));
    }

    /**
     * @param group
     */
    public synchronized void groupChanged(@NonNull final String group) {
        final Tags curGroupTags = Tags.of(STR_GROUP, group);

        if (!m_groupRequestCountMap.containsKey(group)) {
            final Map<RequestType, AtomicLong> groupRequestCountMap = new HashMap<>();

            // initialize request count map
            for (final RequestType curRequestType : RequestType.values()) {
                groupRequestCountMap.put(curRequestType, new AtomicLong(0));
            }

            m_groupRequestCountMap.put(group, groupRequestCountMap);
        }

        // Request counts
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT + CHAR_DOT + STR_TOTAL, curGroupTags, "The total number of " + STR_REQUESTS_FOR_A_GROUP, STR_NO_UNIT, this,
            (m) -> (isRunning() ? getRequestCount(group, RequestType.TOTAL) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_COUNT_REGISTERGROUP.and(curGroupTags), "The number of registerGroup " + STR_REQUESTS_FOR_A_GROUP, STR_NO_UNIT, this,
            (m) -> (isRunning() ? getRequestCount(group, RequestType.REGISTERGROUP) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_COUNT_GETGROUPINFO.and(curGroupTags), "The number of getGroupInfo " + STR_REQUESTS_FOR_A_GROUP, STR_NO_UNIT, this,
            (m) -> (isRunning() ? getRequestCount(group, RequestType.GETGROUPINFO) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_COUNT_HASOBJECTS.and(curGroupTags), "The number of hasObjects " + STR_REQUESTS_FOR_A_GROUP, STR_NO_UNIT, this,
            (m) -> (isRunning() ? getRequestCount(group, RequestType.HASOBJECTS) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_COUNT_GETOBJECTS.and(curGroupTags), "The number of getObjects " + STR_REQUESTS_FOR_A_GROUP, STR_NO_UNIT, this,
            (m) -> (isRunning() ? getRequestCount(group, RequestType.GETOBJECTS) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_COUNT_ADDOBJECTS.and(curGroupTags), "The number of addObjects " + STR_REQUESTS_FOR_A_GROUP, STR_NO_UNIT, this,
            (m) -> (isRunning() ? getRequestCount(group, RequestType.ADDOBJECTS) : 0.0));

        // Cache counts
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_CACHE_MAXCOUNT, curGroupTags, "The maximum number of keys that can be " + STR_CACHED_FOR_A_GROUP, STR_NO_UNIT, this, (m) -> {
            return isRunning() ? m_objectCache.getGroupConfig(group).getMaxKeyCount() : 0.0;
        });

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_CACHE_CURCOUNT, curGroupTags, "The current number of keys that are " + STR_CACHED_FOR_A_GROUP, STR_NO_UNIT, this, (m) -> {
            try {
                return isRunning() ? m_objectCache.getKeyCount(group) : 0.0;
            } catch (@SuppressWarnings("unused") ObjectCacheException e) {
                return 0.0;
            }
        });

        // Cache sizes
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_CACHE_MAXSIZE, curGroupTags, "The maximum size of all objects that can be " + STR_CACHED_FOR_A_GROUP, STR_BYTES, this, (m) -> {
            return isRunning() ? m_objectCache.getGroupConfig(group).getMaxGroupSize() : 0.0;
        });

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_CACHE_CURSIZE, curGroupTags, "The current size of all objects that are " + STR_CACHED_FOR_A_GROUP, STR_BYTES, this, (m) -> {
            try {
                return isRunning() ? m_objectCache.getGroupSize(group) : 0.0;
            } catch (@SuppressWarnings("unused") ObjectCacheException e) {
                return 0.0;
            }
        });

        // Cache oldest key modification age
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_CACHE_OLDEST_MODIFICATION, curGroupTags, "The age of the key with the oldest modification of all keys that are " + STR_CACHED_FOR_A_GROUP, STR_MS, this, (m) -> {
            try {
                if (isRunning()) {
                    return m_objectCache.getMaxKeyAgeMillis(group);
                }
            } catch (@SuppressWarnings("unused") ObjectCacheException e) {
            }

            return 0.0;
        });
    }

    /**
     * !!! Method is not synchronized => ensure that calling method has synchonized the given list !!!
     *
     * @param longValueList
     * @return
     */
    private long implGetMedianValue(@NonNull final LinkedList<Long> longValueList) {
        final int size = longValueList.size();

        if (size < 1) {
            // special case with no values
            return 0;
        } else if (size == 1) {
            // special case with just one value
            return longValueList.getFirst().longValue();
        }

        final Long[] longValueArray = longValueList.toArray(new Long[size]);

        // just sort
        Arrays.sort(longValueArray);

        // determine position of mid value (odd case) or of right value of mid (even case)
        final int valuePos = size >> 1;

        if ((size & 1) == 1) {
            // odd case => return mid value of array
            return longValueArray[valuePos].longValue();
        }

        // even case => return mean value of values left and right of mid
        return Math.round((longValueArray[valuePos - 1].longValue() + longValueArray[valuePos].longValue()) / 2.0);
    }

    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private IObjectCache m_objectCache;

    final private Map<String, Map<RequestType, AtomicLong>> m_groupRequestCountMap = new HashMap<>();

    final private Map<RequestType, LinkedList<Long>> m_requestDurationMap = new HashMap<>();

    final private LinkedList<Long> m_requestDurationAllList = new LinkedList<>();
}
