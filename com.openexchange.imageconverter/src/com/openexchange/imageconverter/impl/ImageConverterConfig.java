/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import static com.openexchange.imageconverter.impl.ImageConverterUtils.LOG;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.config.ConfigurationService;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.objectcache.api.NonNull;

/**
 * {@link ImageConverterConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
/**
 * {@link ImageConverterConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageConverterConfig implements Closeable {

    /**
     * IMAGECONVERTER_TEMP_DIRNAME
     */
    final private static String IMAGECONVERTER_TEMP_DIRNAME = "oxic";

    /**
     * Initializes a new {@link ImageConverterConfig}.
     */
    @SuppressWarnings("unused")
    private ImageConverterConfig() {
        this(null);
    }

    /**
     * Initializes a new {@link ImageConverterConfig}.
     * @param configurationService
     */
    public ImageConverterConfig(final ConfigurationService configurationService) {
        implInit(configurationService);
    }

    /* (non-Javadoc)
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() throws IOException {
        if (IS_TERMINATED.compareAndSet(false, true)) {
            FileUtils.deleteDirectory(IMAGECONVERTER_SPOOLPATH);
        }
    }

    /**
     * @return
     */
    public ImageFormat[] getImageFormats() {
        return m_imageFormats;
    }

    // - Implementation --------------------------------------------------------

    void implInit(final ConfigurationService configService) {
        if (null != configService) {
            // Target formats
            String configTargetFormats = configService.getProperty("com.openexchange.imageconverter.targetFormats", "");

            // if targetFormats property is not set, a default target format list is used
            if (StringUtils.isBlank(configTargetFormats)) {
                configTargetFormats = "auto:200x150, auto:200x150~cover, auto:800x800, auto:1920x1080, auto:1920x1080~cover";
            }

            m_imageFormats = ImageFormat.parseImageFormats(configTargetFormats);

            // Thread count
            IMAGECONVERTER_THREAD_COUNT = Math.max(1, configService.getIntProperty("com.openexchange.imageconverter.threadCount", 8));

            // Queue length
            IMAGECONVERTER_QUEUE_LENGTH = Math.max(1, configService.getIntProperty("com.openexchange.imageconverter.queueLength", 512));

            // Spool directory
            implSetSpoolDir(new File(configService.getProperty("com.openexchange.imageconverter.spoolPath", "/var/spool/open-xchange/imageconverter")));

            // Optional error path
            final String errorPathStr = configService.getProperty("com.openexchange.imageconverter.errorPath");

            if (StringUtils.isNotBlank(errorPathStr)) {
                IMAGECONVERTER_ERROR_PATH = new File(errorPathStr);

                // check, if given path is a writable directory path
                if (!(IMAGECONVERTER_ERROR_PATH.exists() && IMAGECONVERTER_ERROR_PATH.canWrite() && IMAGECONVERTER_ERROR_PATH.isDirectory()) && !IMAGECONVERTER_ERROR_PATH.mkdirs()) {

                    IMAGECONVERTER_ERROR_PATH = null;

                    LOG.warn("IC is not able to create or write to specified error directory: {}", errorPathStr);
                }
            }

            // conversion timeout in milliseconds
            try {
                IMAGECONVERTER_CONVERT_TIMEOUT_MILLIS = Long.parseLong(configService.getProperty("com.openexchange.imageconverter.convertTimeoutMillis", "20000"));

                if (IMAGECONVERTER_CONVERT_TIMEOUT_MILLIS <= 0) {
                    IMAGECONVERTER_CONVERT_TIMEOUT_MILLIS = 20000;
                }
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.error("IC is not able to parse IMAGECONVERTER_CONVERT_TIMEOUT_MILLIS, using default of 20 seconds)");
            }

            // Binary search path
            IMAGECONVERTER_SEARCHPATH = configService.getProperty("com.openexchange.imageconverter.imagemagick.searchPath", "/usr/bin");

            // GrahicsMagick instead of ImageMagick?
            IMAGECONVERTER_USE_GRAPHICSMAGICK = configService.getBoolProperty("com.openexchange.imageconverter.imagemagick.useGraphicsMagick", false);

            // Optional user comment for processed images
            final String userCommentStr = configService.getProperty("com.openexchange.imageconverter.imagemagick.userComment");

            if (StringUtils.isNotBlank(userCommentStr)) {
                IMAGECONVERTER_IMAGE_USERCOMMENT = userCommentStr;
            }

            try {
                // max. cache size in GigaBytes
                IMAGECONVERTER_CACHE_MAX_SIZE = Long.parseLong(configService.getProperty("com.openexchange.imageconverter.cache.maxSizeGB", "-1"));

                if (IMAGECONVERTER_CACHE_MAX_SIZE < 0) {
                    IMAGECONVERTER_CACHE_MAX_SIZE = -1;
                } else {
                    IMAGECONVERTER_CACHE_MAX_SIZE *= (1024 * 1024 * 1024);
                }

                // allow setting in MegaBytes as well; take lowest value of all possible values
                // NOTE: This property is for testing purpose only, not documented!
                long altMaxCacheSize = Long.parseLong(configService.getProperty("com.openexchange.imageconverter.cache.maxSizeMB", "-1"));

                if (altMaxCacheSize >= 0) {
                    altMaxCacheSize *= (1024 * 1024);

                    if ((altMaxCacheSize >= 0) && ((-1 == IMAGECONVERTER_CACHE_MAX_SIZE) || (altMaxCacheSize < IMAGECONVERTER_CACHE_MAX_SIZE))) {
                        IMAGECONVERTER_CACHE_MAX_SIZE = altMaxCacheSize;
                    }
                }
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.error("IC is not able to parse IMAGECONVERTER_CACHE_MAX_SIZE, using default -1");
            }

            try {
                // max. key count (-1 for no upper limit)
                IMAGECONVERTER_CACHE_MAX_KEY_COUNT = Math.max(-1, Long.parseLong(configService.getProperty("com.openexchange.imageconverter.cache.maxKeyCount", "250000")));
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.error("IC is not able to parse IMAGECONVERTER_CACHE_MAX_KEY_COUNT, using default 250000");
            }

            try {
                // key timeout (-1 for no key timeout)
                IMAGECONVERTER_CACHE_KEY_TIMEOUT_MILLIS = Math.max(-1, Long.parseLong(configService.getProperty("com.openexchange.imageconverter.cache.keyTimeoutMinutes", "43200")) * (60 * 1000L));
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.error("IC is not able to parse IMAGECONVERTER_KEY_TIMEOUT_SECONDS, using default of 30 days)");
            }

            try {
                // cleanup timeout (minimum is 1s)
                IMAGECONVERTER_CACHE_CLEANUP_PERIOD_MILLIS = Math.max(1, Long.parseLong(configService.getProperty("com.openexchange.imageconverter.cache.cleanupPeriodSeconds", "300"))) * 1000L;
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.error("IC is not able to parse IMAGECONVERTER_CACHE_CLEANUP_PERIOD_MILLIS, using default of 5 minutes");
            }

            try {
                // uper cleanup threshold in percentage
                IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE = Integer.parseInt(configService.getProperty("com.openexchange.imageconverter.cache.cleanupThresholdUpperPercentage", "100"));

                if (IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE < 0) {
                    IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE = 100;
                }
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.error("IC is not able to parse IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE, using default of 100");
            }

            try {
                // lower cleanup threshold in percentage
                IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE = Integer.parseInt(configService.getProperty("com.openexchange.imageconverter.cache.cleanupThresholdLowerPercentage", "100"));

                if (IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE < 0) {
                    IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE = 100;
                }
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.error("IC is not able to parse IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE, using default of 100");
            }
        }
    }

    /**
     * @param spoolDir
     */
    private static void implSetSpoolDir(@NonNull final File spoolDir) {
        if (null != spoolDir) {
            File targetDir = new File(spoolDir, IMAGECONVERTER_TEMP_DIRNAME);

            // use or create given spool directory, use /tmp as fallback parent directory
            if ((targetDir.exists() && targetDir.canWrite() && targetDir.isDirectory()) ||
                targetDir.mkdirs() || (targetDir = new File("/tmp", IMAGECONVERTER_TEMP_DIRNAME)).mkdirs()) {

                try {
                    FileUtils.cleanDirectory(IMAGECONVERTER_SPOOLPATH = targetDir);
                } catch (@SuppressWarnings("unused") IOException e) {
                    LOG.error("IC is not able to delete spool directory when shut down: {}", IMAGECONVERTER_SPOOLPATH.toString());
                }
            }
        }
    }

    // - Internally used properties --------------------------------------------

    /**
     * DEAFULT_IMAGEFORMATS
     */
    // private final static ImageFormat[] DEFAULT_IMAGEFORMATS = {
    //     ImageFormat.createFrom("auto", true, 128, 128, ScaleType.CONTAIN, true, 75),
    // };

    // - Members ---------------------------------------------------------------

    private ImageFormat[] m_imageFormats; // = DEFAULT_IMAGEFORMATS;

    private static AtomicBoolean IS_TERMINATED = new AtomicBoolean(false);

    // - Public members --------------------------------------------------------

    /**
     * The search ch path for ImageMagick
     */
    public static String IMAGECONVERTER_SEARCHPATH = "/usr/bin";

    /**
     * The spool path (working directory) for ImageMagick
     */
    public static File IMAGECONVERTER_SPOOLPATH = new File("/tmp");

    /**
     * The optional error dir for ImageMagick
     */
    public static File IMAGECONVERTER_ERROR_PATH = null;

    /**
     * Flag to indicate the usage of GraphicsMagick
     */
    public static boolean IMAGECONVERTER_USE_GRAPHICSMAGICK = false;

    /**
     * If set, the given String will be written as comment tag
     * into image file while/when processing an image
     */
    public static String IMAGECONVERTER_IMAGE_USERCOMMENT = "OX_IC";

    /**
     * The maximum amount of time to wait for an image
     * conversion process until it gets terminated
     */
    public static long IMAGECONVERTER_CONVERT_TIMEOUT_MILLIS = 20000;

    /**
     * IMAGECONVERTER_CACHE_MAX_SIZE
     */
    public static long IMAGECONVERTER_CACHE_MAX_SIZE = -1;

    /**
     * IMAGECONVERTER_CACHE_MAX_KEY_COUNT
     */
    public static long IMAGECONVERTER_CACHE_MAX_KEY_COUNT = 250000;

    /**
     * IMAGECONVERTER_CACHE_KEY_TIMEOUT_MILLIS (30 days default)
     */
    public static long IMAGECONVERTER_CACHE_KEY_TIMEOUT_MILLIS = 30 * 24 * 60 * 60 * 1000L;

    /**
     * IMAGECONVERTER_CACHE_CLEANUP_TIMEOUT_MILLIS (5 minutes default)
     */
    public static long IMAGECONVERTER_CACHE_CLEANUP_PERIOD_MILLIS = 5 * 60 * 1000L;

    /**
     * IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE
     */
    public static int IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE = 100;

    /**
     * IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE
     */
    public static int IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE = 100;

    /**
     * IMAGECONVERTER_THREAD_COUNT
     */
    public static int IMAGECONVERTER_THREAD_COUNT = 8;

    /**
     * IMAGECONVERTER_QUEUE_LENGTH
     */
    public static int IMAGECONVERTER_QUEUE_LENGTH = 512;

    /**
     * the location of the log
     */
    public static String IMAGECONVERTER_LOGFILE = null;

    /**
     * determines the amount and detail of logging data;
     * possible values are ERROR, WARN, INFO, DEBUG, TRACE
     * disabled
     */
    public static String IMAGECONVERTER_LOGLEVEL = null;

    /**
     * Internal flag to enable the debug mode
     */
    public static boolean IMAGECONVERTER_DEBUG = false;
}
