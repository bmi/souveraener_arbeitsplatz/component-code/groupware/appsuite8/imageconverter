/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import static com.openexchange.imageconverter.impl.ImageConverterUtils.LOG;
import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import com.google.common.base.Throwables;
import com.openexchange.imageconverter.api.IImageConverter;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageConverterPriority;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.imageconverter.api.MetadataImage;
import com.openexchange.imageconverter.impl.ImageConverterMonitoring.RequestType;
import com.openexchange.objectcache.api.GroupConfig;
import com.openexchange.objectcache.api.ICacheObject;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.IdLocker;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.objectcache.api.ObjectCacheException;
import com.openexchange.osgi.annotation.SingletonService;

/**
 * {@link ImageConverter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
/**
 * {@link ImageConverter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public final class ImageConverter implements IImageConverter {

    /**
     * Initializes a new {@link ImageConverter}.
     *
     * @param objectCache
     * @param config
     */
    @SingletonService
    public ImageConverter(
        @NonNull final IObjectCache objectCache,
        @NonNull final IMetadataReader metadataReader,
        @NonNull final ImageConverterConfig imageConverterConfig) throws ImageConverterException {

        super();

        m_objectCache = objectCache;
        m_metadataReader = metadataReader;

        final ImageFormat[] configImageFormats = imageConverterConfig.getImageFormats();

        // the given image Format list has to contains at least one valid entry
        if (configImageFormats.length < 1) {
            throw new ImageConverterException("IC detected ImageFormat array with no valid entries => ensure at least one valid ImageFormat contained");
        }

        Arrays.sort(configImageFormats);
        m_imageFormats = Arrays.asList(configImageFormats);

        // create ImageProcessor
        m_imageProcessor = new ImageProcessor(
            ImageConverterConfig.IMAGECONVERTER_SEARCHPATH,
            ImageConverterConfig.IMAGECONVERTER_USE_GRAPHICSMAGICK,
            ImageConverterConfig.IMAGECONVERTER_IMAGE_USERCOMMENT);

        if ((null != m_objectCache) && m_objectCache.isValid()) {
            try {
                // register group at ObjectCache
                final GroupConfig groupConfig = GroupConfig.builder().
                    withGroupId(ImageConverterUtils.IMAGECONVERTER_GROUPID).
                    withCustomKeys(ImageConverterUtils.IMAGECONVERTER_KEYS).
                    withMaxGroupSize(ImageConverterConfig.IMAGECONVERTER_CACHE_MAX_SIZE).
                    withMaxKeyCount(ImageConverterConfig.IMAGECONVERTER_CACHE_MAX_KEY_COUNT).
                    withKeyTimeoutMillis(ImageConverterConfig.IMAGECONVERTER_CACHE_KEY_TIMEOUT_MILLIS).
                    withCleanupPeriodMillis(ImageConverterConfig.IMAGECONVERTER_CACHE_CLEANUP_PERIOD_MILLIS).
                    withCleanupThresholdUpperPercentage(ImageConverterConfig.IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE).
                    withCleanupThresholdLowerPercentage(ImageConverterConfig.IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE).build();

                m_objectCache.registerGroup(groupConfig);
            } catch (ObjectCacheException e) {
                throw new ImageConverterException("IC error when registering group at ObjectCache", e);
            }

            // create and start worker queue to perform the image transformations (either synchronously or asynchrnously)
            m_queue = new ImageConverterQueue(this, m_imageProcessor, m_objectCache, m_metadataReader);
        } else {
            m_queue = null;

            LOG.error(ImageConverterUtils.IC_STR_BUILDER().
                append("got no valid ObjectCache interface => Falling back to on demand conversion for cacheAndGet* requests without cache! ").
                append("Please setup the ObjectCache correctly to speed up performance before next service restart.").
                toString());
        }

        // Create ImageConverterDirect optional member to be used for non cached fallback conversions
        m_imageConverterDirect = m_imageProcessor.isAlive() ?
            new ImageConverterDirect(m_imageProcessor, metadataReader, m_imageFormats) :
                null;

        // finally trace list of used image formats
        if (LOG.isInfoEnabled()) {
            final StringBuilder infoStr = ImageConverterUtils.IC_STR_BUILDER().
                append("is using the following target formats:\n");

            for (final ImageFormat curImageFormat : m_imageFormats) {
                infoStr.append("\tIC ").append(curImageFormat.toString()).append('\n');
            }

            LOG.info(infoStr.toString());
        }

        // initialize ImageConverterUtils with this singleton object
        ImageConverterUtils.init(this);

        implKillOrphanedProcesses();
    }

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            final long shutdownStartTimeMillis = System.currentTimeMillis();

            LOG.info("IC starting shutdown");

            if (null != m_objectCache) {
                m_objectCache.shutdownGroup(ImageConverterUtils.IMAGECONVERTER_GROUPID);
            }

            // might be null in case no ObjectCache is available
            if (null != m_queue) {
                m_queue.shutdown();
            }

            implKillOrphanedProcesses();

            LOG.info("IC shutdown finished in {}ms", (System.currentTimeMillis() - shutdownStartTimeMillis));
        }
    }

    /**
     * @return
     */
    public boolean isRunning() {
        return m_running.get();
    }

    /**
     * @return
     */
    public ImageProcessor.ImageProcessorStatus getImageProcessorStatus() {
        return m_imageProcessor.getStatus();
    }

    // - IImageConverter ----------------------------------------------------------

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getImage(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public InputStream getImage(final String imageKey, final String requestFormat, final String... context) throws ImageConverterException {
        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace(ImageConverterUtils.STR_BUILDER().
                append("#getImage called (imageKey / requestFormat): ").
                append(imageKey).append(" / ").append(requestFormat).toString(),
                context);
        }

        ImageConverterStatus status = (isNotEmpty(imageKey) && isNotEmpty(requestFormat)) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            // no cache operation possible ATM => return null
            if ((null == m_queue) || !m_cacheStatusOK.get()) {
                status = ImageConverterStatus.CACHE_NOT_AVAILABLE;
                return null;
            } else {
                try (final MetadataImage metadataImage = m_queue.getMetadataImage(imageKey, requestFormat)) {
                    InputStream ret = (null != metadataImage) ?
                        metadataImage.getImageInputStream(true) :
                            null;

                    if (null == ret) {
                        status = ImageConverterStatus.KEY_NOT_AVAILABLE;
                    }

                    return ret;
                } catch (IOException | ImageConverterException e) {
                    LOG.trace("IC exception while getting MetadataImage: {}", Throwables.getRootCause(e).getMessage());

                    status = ImageConverterStatus.GENERAL_ERROR;
                    throw new ImageConverterException("IC error while getting MetadataImage: " + imageKey, e);
                }
            }
        } finally {
            implFinishRequest(RequestType.GET, requestStartTime, status);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverter#getMetadata(java.lang.String, java.lang.String[])
     */
    @Override
    public IMetadata getMetadata(String imageKey, String... context) throws ImageConverterException {
        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace(ImageConverterUtils.STR_BUILDER().
                append("#getMetadata called (imageKey / requestFormat): ").
                append(imageKey).toString(),
                context);
        }

        ImageConverterStatus status = isNotEmpty(imageKey) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            // no cache operation possible ATM => return null
            if ((null == m_queue) || !m_cacheStatusOK.get()) {
                status = ImageConverterStatus.CACHE_NOT_AVAILABLE;
                return null;
            }

            final IMetadata ret = (ImageConverterStatus.OK == status) ?
                m_queue.getMetadata(imageKey) :
                    null;

            if (null == ret) {
                status = ImageConverterStatus.KEY_NOT_AVAILABLE;
            }

            return ret;
        } catch (ImageConverterException e) {
            LOG.trace("IC exception while getting Metadata: {}", Throwables.getRootCause(e).getMessage());

            status = ImageConverterStatus.GENERAL_ERROR;
            throw e;
        } finally {
            implFinishRequest(RequestType.GET, requestStartTime, status);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverter#getImageAndMetadata(java.lang.String, java.lang.String, java.lang.String[])
     */
    @Override
    public MetadataImage getImageAndMetadata(String imageKey, String requestFormat, String... context) throws ImageConverterException {
        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace(ImageConverterUtils.STR_BUILDER().
                append("#getImageAndMetadata called (imageKey / requestFormat): ").
                append(imageKey).append(" / ").append(requestFormat).toString(),
                context);
        }

        ImageConverterStatus status = isNotEmpty(imageKey) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            // no cache operation possible ATM => return null
            if ((null == m_queue) || !m_cacheStatusOK.get()) {
                status = ImageConverterStatus.CACHE_NOT_AVAILABLE;
                return null;
            }

            final MetadataImage ret = (ImageConverterStatus.OK == status) ?
                m_queue.getMetadataImage(imageKey, requestFormat) :
                    null;

            if (null == ret) {
                status = ImageConverterStatus.KEY_NOT_AVAILABLE;
            }

            return ret;
        } catch (ImageConverterException e) {
            LOG.trace("IC exception while getting ImageAndMetadata: {}", Throwables.getRootCause(e).getMessage());

            status = ImageConverterStatus.GENERAL_ERROR;
            throw e;
        } finally {
            implFinishRequest(RequestType.GET, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#cacheImage(java.lang.String, java.lang.String, java.io.InputStream)
     */
    @Override
    public void cacheImage(final String imageKey, final InputStream inputStm, final String... context) throws ImageConverterException {
        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace("#cacheImage called for imageKey: " + imageKey, context);
        }

        ImageConverterStatus status = (isNotEmpty(imageKey) && (null != inputStm)) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            // no cache operation => do nothing
            if (null == m_queue) {
                status = ImageConverterStatus.CACHE_NOT_AVAILABLE;
                return;
            }

            if (ImageConverterStatus.OK == status) {
                m_queue.cacheAndGetMetadataImage(imageKey, inputStm, ImageConverterPriority.BACKGROUND, null, implGetContext(context), m_cacheStatusOK.get());
            }
        } catch (ImageConverterException e) {
            LOG.trace("IC exception while caching Image: {}", Throwables.getRootCause(e).getMessage());

            status = ImageConverterStatus.GENERAL_ERROR;
            throw e;
        } finally {
            implFinishRequest(RequestType.CACHE, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getImage(java.lang.String, java.lang.String, java.lang.String, java.io.InputStream)
     */
    @Override
    public InputStream cacheAndGetImage(final String imageKey, final String requestFormat, final InputStream inputStm, final String... context) throws ImageConverterException {
        return cacheAndGetImage(imageKey, requestFormat, inputStm, new Mutable<>(Boolean.TRUE), context);
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverter#cacheAndGetImageAndMetadata(java.lang.String, java.lang.String, java.io.InputStream, java.lang.String[])
     */
    @Override
    public MetadataImage cacheAndGetImageAndMetadata(String imageKey, String requestFormat, InputStream inputStm, String... context) throws ImageConverterException {
        return cacheAndGetImageAndMetadata(imageKey, requestFormat, inputStm, new Mutable<>(Boolean.TRUE), context);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#clearImages()
     */
    @Override
    public void clearImages(final String... context) throws ImageConverterException {
        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace("#clearImages called", context);
        }

        ImageConverterStatus status = ImageConverterStatus.OK;

        try {
            remove(Arrays.asList(getAvailable(implCreateContextProperties(context))));
        } catch (ImageConverterException e) {
            LOG.trace("IC exception while clearing Images: {}", Throwables.getRootCause(e).getMessage());

            status = ImageConverterStatus.GENERAL_ERROR;
            throw e;
        } finally {
            implFinishRequest(RequestType.ADMIN, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#clearImagesByKey(java.lang.String)
     */
    @Override
    public void clearImagesByKey(String imageKey) throws ImageConverterException {
        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace("#clearImagesByKey called for imageKey: " + imageKey);
        }

        ImageConverterStatus status = isNotEmpty(imageKey) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            if (ImageConverterStatus.OK == status) {
                remove(imageKey);
            }
        } catch (ImageConverterException e) {
            LOG.trace("IC exception while clearing Images by key: {}", Throwables.getRootCause(e).getMessage());

            status = ImageConverterStatus.GENERAL_ERROR;
            throw e;
        } finally {
            implFinishRequest(RequestType.ADMIN, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getKeyCount()
     */
    @Override
    public long getKeyCount(final String... context) throws ImageConverterException {
        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace("#getKeyCount called", context);
        }

        ImageConverterStatus status = ImageConverterStatus.OK;

        try {
            return isEmpty(context) ?
                getKeyCount() :
                    getAvailable(implCreateContextProperties(context)).length;
        } catch (ImageConverterException e) {
            status = ImageConverterStatus.GENERAL_ERROR;
            LOG.trace("IC exception while getting KeyCount: {}", Throwables.getRootCause(e).getMessage());
            throw e;
        } finally {
            implFinishRequest(RequestType.ADMIN, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getKeysByContext(java.lang.String)
     */
    @Override
    public String[] getKeys(final String... context) throws ImageConverterException {
        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace("IC #getKeys called", context);
        }

        ImageConverterStatus status = ImageConverterStatus.OK;

        try {
            return getAvailable(implCreateContextProperties(context));
        } catch (ImageConverterException e) {
            status = ImageConverterStatus.GENERAL_ERROR;
            LOG.trace("IC exception while getting Keys: {}", Throwables.getRootCause(e).getMessage());
            throw e;
        } finally {
            implFinishRequest(RequestType.ADMIN, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getKeyCount()
     */
    @Override
    public long getTotalImagesSize(final String... context) throws ImageConverterException {
        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace("#getTotalImagesSize called", context);
        }

        ImageConverterStatus status = ImageConverterStatus.OK;

        try {
            return isEmpty(context) ?
                getTotalImagesSize() :
                    getAvailableImagesSize(implCreateContextProperties(context));
        } catch (ImageConverterException e) {
            LOG.trace("IC exception while getting TotalImagesSize: {}", Throwables.getRootCause(e).getMessage());

            status = ImageConverterStatus.GENERAL_ERROR;
            throw e;
        } finally {
            implFinishRequest(RequestType.ADMIN, requestStartTime, status);
        }
    }

    // - Interface -------------------------------------------------------------

    /**
     * @return
     */
    public List<ImageFormat> getImageFormats() {
        return m_imageFormats;
    }

    /**
     * @return
     */
    public @Nullable IObjectCache getFileItemService() {
        return m_objectCache;
    }

    /**
     * @return
     */
    public IMetadataReader getMetadataReader() {
        return m_metadataReader;
    }

    /**
     * @return
     * @throws ImageConverterException
     */
    public long getKeyCount() throws ImageConverterException {
        try {
            return (null != m_objectCache) ?
                m_objectCache.getKeyCount(ImageConverterUtils.IMAGECONVERTER_GROUPID) :
                    0;
        } catch (ObjectCacheException e) {
            throw new ImageConverterException("IC error while getting image key count", e);
        }
    }

    /**
     * @return
     */
    public long getTotalImagesSize() throws ImageConverterException {
        try {
            return (null != m_objectCache) ?
                m_objectCache.getGroupSize(ImageConverterUtils.IMAGECONVERTER_GROUPID) :
                    0;
        } catch (ObjectCacheException e) {
            throw new ImageConverterException("IC error while getting total images size", e);
        }
    }

    /**
     * @param searchProperties
     * @return
     * @throws ImageConverterException
     */
    public long getAvailableImagesSize(@Nullable final Properties searchProperties) throws ImageConverterException {
        try {
            return (null != m_objectCache) ?
                m_objectCache.getGroupSize(ImageConverterUtils.IMAGECONVERTER_GROUPID, searchProperties) :
                    0;
        } catch (ObjectCacheException e) {
            throw new ImageConverterException("IC error while getting available images size", e);
        }
    }

    /**
     * @param imageKey
     * @return
     * @throws ImageConverterException
     */
    public boolean isAvailable(@NonNull final String imageKey) throws ImageConverterException {
        try {
            return (null != m_objectCache) ?
                m_objectCache.containsKey(ImageConverterUtils.IMAGECONVERTER_GROUPID, imageKey) :
                    false;
        } catch (ObjectCacheException e) {
            throw new ImageConverterException("IC error while checking availability: " + imageKey, e);
        }
    }

    /**
     * @param searchProperties
     * @return
     * @throws ImageConverterException
     */
    @NonNull public String[] getAvailable(@Nullable final Properties searchProperties) throws ImageConverterException {
        if (null != m_objectCache) {
            try {
                final Object[] foundObjs = (null != searchProperties) ?
                    m_objectCache.get(ImageConverterUtils.IMAGECONVERTER_GROUPID, searchProperties) :
                        m_objectCache.getKeys(ImageConverterUtils.IMAGECONVERTER_GROUPID);

                if (isNotEmpty(foundObjs)) {
                    final String[] ret = new String[foundObjs.length];
                    final Set<String> imageKeySet = (null != searchProperties) ? new HashSet<>() : null;
                    int count = 0;

                    for (final Object curObj : foundObjs) {
                        if (null != curObj) {
                            final String curImageKey = (curObj instanceof ICacheObject) ?
                                ((ICacheObject) curObj).getKeyId() :
                                    curObj.toString();

                            if (isNotEmpty(curImageKey) && ((null == imageKeySet) || imageKeySet.add(curImageKey))) {
                                ret[count++] = curImageKey;
                            }
                        }
                    }

                    return ArrayUtils.subarray(ret, 0, count);
                }
            } catch (ObjectCacheException e) {
                throw new ImageConverterException("IC error while retrieving available image keys", e);
            }
        }

        return ArrayUtils.EMPTY_STRING_ARRAY;
    }

    /**
     * @param imageKey
     * @param inputStm
     * @param context
     * @return
     */
    public void remove(@NonNull final String imageKey) throws ImageConverterException {
        if (m_running.get() && (null != m_queue)) {
            if (IdLocker.lock(imageKey, IdLocker.Mode.TRY_LOCK)) {
                try {
                    // only remove keys that are not currently processed
                    if (!m_queue.isProcessing(imageKey)) {
                        m_objectCache.removeKey(ImageConverterUtils.IMAGECONVERTER_GROUPID, imageKey);
                    }
                } catch (ObjectCacheException e) {
                    throw new ImageConverterException("IC error while removing image key: " + imageKey, e);
                } finally {
                    IdLocker.unlock(imageKey);
                }
            }
        }
    }

    /**
     * @param imageKeys
     * @throws ImageConverterException
     */
    public void remove(@NonNull final Collection<String> imageKeysCollection) throws ImageConverterException {
        if (!imageKeysCollection.isEmpty() && m_running.get() && (null != m_queue)) {
            final String[] targetImageKeys = imageKeysCollection.stream().
                filter((curImageKey) -> {
                    if (IdLocker.lock(curImageKey, IdLocker.Mode.TRY_LOCK)) {
                        // only remove keys that are not currently processed
                        if (m_queue.isProcessing(curImageKey)) {
                            IdLocker.unlock(curImageKey);
                            return false;
                        }

                        // current key needs to be unlocked after finishing removal
                        return true;
                    }

                    return false;
                }).
                toArray((imageKeyCount) -> new String[imageKeyCount]);

            if (isNotEmpty(targetImageKeys)) {
                try {
                    m_objectCache.removeKeys(ImageConverterUtils.IMAGECONVERTER_GROUPID, targetImageKeys);
                } catch (ObjectCacheException e) {
                    throw new ImageConverterException("IC error while removing image " + targetImageKeys.length + " keys", e);
                } finally {
                    for (final String curImageKey : targetImageKeys) {
                        IdLocker.unlock(curImageKey);
                    }
                }
            }
        }
    }

    /**
     * @param imageKey
     * @param requestFormat
     * @param inputStm
     * @param context
     * @return
     */
    public InputStream cacheAndGetImage(final String imageKey,
        final String requestFormat,
        final InputStream inputStm,
        final Mutable<Boolean> usedCache,
        final String... context) throws ImageConverterException {

        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace(ImageConverterUtils.STR_BUILDER().
                append("#cacheAndGetImage called (imageKey / requestFormat): ").
                append(imageKey).append(" / ").append(requestFormat).toString(),
                context);
        }

        ImageConverterStatus status = isNotEmpty(imageKey) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            final byte[] inputBuffer = IOUtils.toByteArray(inputStm);

            // try cache based operation first
            if ((null != m_queue) && (ImageConverterStatus.OK == status)) {
                try (
                    // only remove entry in error case completely if cache is accesible
                    final InputStream bufferInputStm = new ByteArrayInputStream(inputBuffer);
                    final MetadataImage metadataImage = (null != bufferInputStm) ?
                    m_queue.cacheAndGetMetadataImage(imageKey, bufferInputStm, ImageConverterPriority.BACKGROUND, requestFormat, implGetContext(context), m_cacheStatusOK.get()) :
                        (isNotEmpty(requestFormat) ?
                            m_queue.getMetadataImage(imageKey, requestFormat) :
                                null)) {

                    InputStream ret = (null != metadataImage) ? metadataImage.getImageInputStream(true) : null;

                    if (null == ret) {
                        status = ImageConverterStatus.KEY_NOT_AVAILABLE;
                    }

                    usedCache.set(Boolean.TRUE);
                    implLogChangedCacheStatus(true);

                    return ret;
                } catch (IOException | ImageConverterException e) {
                    LOG.trace("IC exception while caching and getting Image: {}", Throwables.getRootCause(e).getMessage());
                    status = ImageConverterStatus.GENERAL_ERROR;
                }
            }

            // no cache based conversion or exception during cache based queue conversion => use on demand conversion, if possible
            if ((null != m_imageConverterDirect) && (null != inputBuffer)) {
                final ImageFormat targetFormat = ImageFormat.parseImageFormat(requestFormat);

                try (
                    final InputStream bufferInputStm = new ByteArrayInputStream(inputBuffer);
                    final MetadataImage metadataImage = m_imageConverterDirect.convert(bufferInputStm, targetFormat)) {

                    if (null != metadataImage) {
                        usedCache.set(Boolean.FALSE);
                        implLogChangedCacheStatus(false);

                        return metadataImage.getImageInputStream(true);
                    }
                } catch (Exception e) {
                    LOG.trace("IC exception while performing on demand image conversion in #cacheAndGetImage: {}", Throwables.getRootCause(e).getMessage());
                }
            }
        } catch (IOException e) {
            LOG.error("IC exception while creating input buffer in #cacheAndGetImage: {}", Throwables.getRootCause(e).getMessage());
        } finally {
            implFinishRequest(RequestType.CACHE_AND_GET, requestStartTime, status);
        }

        return null;
    }

    /**
     * @param imageKey
     * @param requestFormat
     * @param inputStm
     * @param context
     * @param usedCache
     * @return
     * @throws ImageConverterException
     */
    public MetadataImage cacheAndGetImageAndMetadata(final String imageKey,
        final String requestFormat,
        final InputStream inputStm,
        final Mutable<Boolean> usedCache,
        final String... context) throws ImageConverterException {

        final long requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            implTrace(ImageConverterUtils.STR_BUILDER().
                append("#cacheAndGetImageAndMetadata called (imageKey / requestFormat): ").
                append(imageKey).append(" / ").append(requestFormat).toString(),
                context);
        }

        ImageConverterStatus status = isNotEmpty(imageKey) ?
            (((null != inputStm) || isNotEmpty(requestFormat)) ? ImageConverterStatus.OK : ImageConverterStatus.KEY_NOT_AVAILABLE) :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            final byte[] inputBuffer = IOUtils.toByteArray(inputStm);

            // try cache based operation first
            if ((null != m_queue) && (ImageConverterStatus.OK == status)) {
                try {
                    usedCache.set(Boolean.TRUE);
                    implLogChangedCacheStatus(true);

                    try (final InputStream bufferInputStm = new ByteArrayInputStream(inputBuffer)) {
                        // only remove entry in error case completely if cache is accesible
                        return (null != inputBuffer) ?
                            m_queue.cacheAndGetMetadataImage(imageKey, bufferInputStm, ImageConverterPriority.BACKGROUND, requestFormat, implGetContext(context), m_cacheStatusOK.get()) :
                                m_queue.getMetadataImage(imageKey, requestFormat);
                    }
                } catch (ImageConverterException e) {
                    LOG.trace("IC exception while caching and getting ImageAndMetadata: {}", Throwables.getRootCause(e).getMessage());
                    status = ImageConverterStatus.GENERAL_ERROR;
                }
            }

            // no cache based conversion or exception during cache based queue conversion => use on demand conversion, if possible
            if ((null != m_imageConverterDirect) && (null != inputBuffer)) {
                final ImageFormat targetFormat = ImageFormat.parseImageFormat(requestFormat);

                try (final InputStream bufferInputStm = new ByteArrayInputStream(inputBuffer)) {
                    final MetadataImage ret = m_imageConverterDirect.convert(bufferInputStm, targetFormat);

                    if (null != ret) {
                        usedCache.set(Boolean.FALSE);
                        implLogChangedCacheStatus(false);

                        return ret;
                    }
                } catch (Exception e) {
                    LOG.trace("IC exception while performing on demand image conversion in #cacheAndGetImageAndMetadata: {}", Throwables.getRootCause(e).getMessage());
                }
            }
        } catch (IOException e) {
            LOG.error("IC exception while creating input buffer in #cacheAndGetImage: {}", Throwables.getRootCause(e).getMessage());
        } finally {
            implFinishRequest(RequestType.CACHE_AND_GET, requestStartTime, status);
        }

        return null;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param requestType
     * @param requestStartTime
     * @param imageConverterStatus
     */
    private static void implFinishRequest(@NonNull final RequestType requestType, final long requestStartTime, @NonNull final ImageConverterStatus imageConverterStatus) {
        final long requestDuration = System.currentTimeMillis() - requestStartTime;

        if (LOG.isTraceEnabled()) {
            implTrace(ImageConverterUtils.STR_BUILDER().
                append("request type ").
                append(requestType.name()).
                append(" finished in ").
                append(requestDuration).append(" ms").toString());
        }

        ImageConverterUtils.IC_MONITOR.incrementRequests(requestType, requestDuration, imageConverterStatus);
    }

    /**
     *
     */
    private void implKillOrphanedProcesses() {
        try {
            // perform cleanup of possible orphaned convert processes
            ImageProcessor.killOrphanedProcess(true);
        } catch (Exception e) {
            LOG.error("IC received Exception when calling orphaned process cleanup task: {}", Throwables.getRootCause(e).getMessage());
        }
    }

    /**
     *
     */
    private void implLogChangedCacheStatus(final boolean usedCache) {
        if (usedCache) {
            if (m_cacheStatusOK.compareAndSet(false, true)) {
                LOG.info(ImageConverterUtils.IC_STR_BUILDER().
                    append("(re)enabled cache based image operation.").toString());
            }
        } else if (m_cacheStatusOK.compareAndSet(true, false)) {
            LOG.warn(ImageConverterUtils.IC_STR_BUILDER().
                append("disabled cache based image operation. Using direct on demand conversion instead!").toString());
        }
    }

    /**
     * @param context
     * @return
     */
    private static Properties implCreateContextProperties(final String... context) {
        final String usedContext = implGetContext(context);
        Properties ret = null;

        if (null != usedContext) {
            (ret = new Properties()).setProperty(ImageConverterUtils.IMAGECONVERTER_KEY_CONTEXT, implGetContext(context));
        }

        return ret;
    }

    /**
     * @param context
     * @return
     */
    private static String implGetContext(final String... context) {
        return isNotEmpty(context) && isNotEmpty(context[0]) ? context[0] : null;
    }

    /**
     * @param traceMsg
     * @param context
     */
    private static void implTrace(@NonNull final String traceMsg, final String... context) {
        LOG.trace(isNotEmpty(context) ?
            ImageConverterUtils.IC_STR_BUILDER().append(traceMsg).append( "(Context: ").append(context[0]).append(")").toString() :
                traceMsg);
    }

    // - Members ---------------------------------------------------------------

    final private IObjectCache m_objectCache;

    final private ImageConverterDirect m_imageConverterDirect;

    final private IMetadataReader m_metadataReader;

    final private List<ImageFormat> m_imageFormats;

    final private ImageProcessor m_imageProcessor;

    final private ImageConverterQueue m_queue;

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private AtomicBoolean m_cacheStatusOK = new AtomicBoolean(true);
}
