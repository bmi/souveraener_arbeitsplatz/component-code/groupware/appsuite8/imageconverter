/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Throwables;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.objectcache.api.ICacheObject;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.IReadAccess;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link ImageConverterUtils}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
/**
 * {@link ImageConverterUtils}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageConverterUtils {

    final public static Logger LOG = LoggerFactory.getLogger(ImageConverter.class);

    /**
     * In order to initialize both following variables the {@link ImageConverterUtils.init}
     * method has to be called as soon as the ImageConverter singleton has been created
     * (preferable from the Ctor of the ImageConverter singleton itself)
     *
     * IC
     */
    public static ImageConverter IC = null;

    /**
     * IC_MBEAN
     */
    public static ImageConverterMBean IC_MBEAN = null;

    /**
     * IC_METRCS
     */
    public static ImageConverterMetrics IC_METRICS = null;

    /**
     * IC_MONITOR
     */
    public static ImageConverterMonitoring IC_MONITOR = null;


    /**
     * IMAGECONVERTER_FILEGROUP: do no change!!!
     */
    final public static String IMAGECONVERTER_GROUPID = "OX_IC";

    /**
     * IMAGECONVERTER_FILEGROUP: do no change!!!
     */
    final public static String IMAGECONVERTER_METADATA_FILEID = "OX_ICMETA";

    /**
     * IMAGECONVERTER_KEY_CONTEXT: do no change!!!
     */
    final public static String IMAGECONVERTER_KEY_CONTEXT = "CTX";

    /**
     * IMAGECONVERTER_KEY_REFERENCE: do no change!!!
     */
    final public static String IMAGECONVERTER_KEY_REFERENCE = "REF";

    /**
     * IMAGECONVERTER_KEYS: sorted order is guaranteed
     */
    final public static String[] IMAGECONVERTER_KEYS = { IMAGECONVERTER_KEY_CONTEXT, IMAGECONVERTER_KEY_REFERENCE };

    /**     * MAX_IMAGE_EXTENT
     */
    final public static int MAX_IMAGE_EXTENT = 8192;

    /**
     * AWAIT_TERMINATION_TIMEOUT_MILLIS
     */
    final public static long AWAIT_TERMINATION_TIMEOUT_MILLIS = 2000;

    /**
     * Initializes a new {@link ImageConverterUtils}.
     */
    private ImageConverterUtils() {
        super();
    }

    /**
     * This method needs to be called as soon as an ImageConverter has been created
     *
     * @param imageConverter
     */
    public static void init(@NonNull final ImageConverter imageConverter) {
        IC_MONITOR = new ImageConverterMonitoring(IC = imageConverter);
        IC_METRICS = new ImageConverterMetrics(IC_MONITOR);
    }

    /**
     * @return
     */
    public static StringBuilder STR_BUILDER() {
        return new StringBuilder(256);
    }

    /**
     * @return
     */
    public static StringBuilder IC_STR_BUILDER() {
        return STR_BUILDER().append("IC ");
    }

    /**
    * @return
    */
    public static @Nullable File createTempFile() {
        try {
            return File.createTempFile(TEMP_COUNTER.incrementAndGet() + "oxic", ".tmp", ImageConverterConfig.IMAGECONVERTER_SPOOLPATH);
        } catch (IOException e) {
            LOG.error("IC can not create temp. file: {}", Throwables.getRootCause(e).getMessage());
        }
        return null;
    }

    /**
     * @return
     */
    public static @Nullable File createTempDir() {
        try {
            final Path tmpDirPath = Files.createTempDirectory(
                ImageConverterConfig.IMAGECONVERTER_SPOOLPATH.toPath(),
                TEMP_COUNTER.incrementAndGet() + "oxicd");

            if (null != tmpDirPath) {
                return tmpDirPath.toFile();
            }
        } catch (Exception e) {
            LOG.error("IC can not create temp. directory: {}", Throwables.getRootCause(e).getMessage());
        }

        return null;

    }

    /**
     * @param availableFormats
     * @param requestFormat
     * @return
     */
    public static ImageFormat getBestMatchingFormat(@NonNull final List<ImageFormat> imageFormats, @NonNull IMetadata imageMetadata, @NonNull ImageFormat requestFormat) {
        final boolean trace = LOG.isTraceEnabled();
        ImageFormat ret = null;

        if (imageFormats.size() > 0) {
            // Type safety warning has been checked!
            @SuppressWarnings("unchecked")
            final TreeSet<ImageFormat> candidateSet = (TreeSet<ImageFormat>) m_emptyCandidateSet.clone();
            final BitSet curTestSet = (BitSet) m_paramTestSet.clone();
            final Dimension imageDimension = imageMetadata.getImageDimension();
            final List<ImageFormat> workingImageFormatsList = new ArrayList<>(imageFormats.size());

            workingImageFormatsList.addAll(imageFormats);

            if (requestFormat.getWidth() <= 0) {
                requestFormat.setWidth(imageDimension.width);
            }

            if (requestFormat.getHeight() <= 0) {
                requestFormat.setHeight(imageDimension.height);
            }

            // iterate over all left entries within the imageFormatList
            // and add all entries, whose to be tested parameters, defined
            // in the test bit set, match the equivalent requestFormat parameters;
            // leave the loop, if the candidate list contains at least one entry;
            // calling #implCreateImageFormatCandidateList with an empty
            // test set adds all entries from the current imageFormatList
            do {
                implFillImageFormatCandidateList(workingImageFormatsList, curTestSet, requestFormat, candidateSet);

                if (curTestSet.isEmpty()) {
                    break;
                }

                // reduce the to be test parameters set by the
                // last entry, the set entries are added
                // with decreasing performance;
                // this means, that with each iteration within
                // this loop, the set of parameters that must
                // match is reduced, getting at least one
                // entry within the candidate list
                curTestSet.clear(curTestSet.length() - 1);
            } while (candidateSet.isEmpty());

            if (!candidateSet.isEmpty()) {
                final Dimension requestDimension = getResultingImageDimension(imageDimension, requestFormat);

                // iterate over all target formats from the
                // candidate set and find best matching
                for (final @NonNull ImageFormat curTargetFormat : candidateSet) {
                    final Dimension curResultingTargetDimension = getResultingImageDimension(imageDimension, curTargetFormat);

                    // search until candidates' format dimension is equal
                    // or larger than the requests' format dimension
                    if ((curResultingTargetDimension.width >= requestDimension.width) &&
                        (curResultingTargetDimension.height >= requestDimension.height)) {

                        ret = curTargetFormat;
                        break;
                    }
                }

                if (null == ret) {
                    // take largest possible dimension, in case no candidate matched so far
                    ret = candidateSet.last();
                }
            }

            if (null == ret) {
                ret = imageFormats.get(imageFormats.size() - 1);

                // this should not happen at all, if a valid ImageFormat list is provided
                LOG.error(IC_STR_BUILDER().
                    append("found no best matching image format for requested format, using target format: ").
                    append(requestFormat.getFormatString()).append(" => ").
                    append(ret.getFormatString()).toString());
            }

            if (trace) {
                LOG.trace(IC_STR_BUILDER().append("found best matching image format for requested format: ").
                    append(requestFormat.getFormatString()).append(" => ").append(ret.getFormatString()).toString());
            }
        } else if (LOG.isWarnEnabled()) {
            LOG.warn("IC image format list has no valid entries => please check configuration of imageconverter.properties");
        }
        if (ret != null) {
            ImageConverterUtils.IC_MONITOR.registerAndIncreaseImageFormatRequestCount(requestFormat, ret.getWidth(), ret.getHeight(), imageMetadata.getImageFormatName());
        }
        return ret;
    }

    /**
     * @param extent
     * @param positiveOnly
     * @return
     */
    public static Integer getIM4JExtent(final int extent) {
        return Integer.valueOf((extent > 0) ? Math.min(extent, MAX_IMAGE_EXTENT) : -1);
    }

    /**
     * @param imageDimension
     * @param targetFormat
     * @return
     */
    public static Dimension getResultingImageDimension(final Dimension imageDimension, final ImageFormat targetFormat) {
        final double targetWidth = targetFormat.getWidth();
        final double targetHeight = targetFormat.getHeight();
        int imgDstWidth = imageDimension.width;
        int imgDstHeight = imageDimension.height;

        if ((targetWidth > 0) && (targetHeight > 0) && (imgDstWidth > 0) && (imgDstHeight > 0)) {
            final double widthScaleFactor = targetWidth / imageDimension.width;
            final double heightScaleFactor = targetHeight / imageDimension.height;

            switch (targetFormat.getScaleType()) {
                case CONTAIN: {
                    // Choose smallest boundary
                    double scaleFactor = Math.min(widthScaleFactor, heightScaleFactor);

                    if (targetFormat.isShrinkOnly()) {
                        scaleFactor = Math.min(scaleFactor, 1.0);
                    }

                    imgDstWidth = (int) Math.round(imgDstWidth * scaleFactor);
                    imgDstHeight = (int) Math.round(imgDstHeight * scaleFactor);

                    break;
                }

                case COVER: {
                    // Choose largest boundary
                    double scaleFactor = Math.max(widthScaleFactor, heightScaleFactor);

                    imgDstWidth = (int) Math.round(imgDstWidth * scaleFactor);
                    imgDstHeight = (int) Math.round(imgDstHeight * scaleFactor);

                    break;
                }

                case AUTO:
                case COVER_AND_CROP:
                case CONTAIN_FORCE_DIMENSION:
                default: {
                    imgDstWidth = (int) targetWidth;
                    imgDstHeight = (int) targetHeight;

                    break;
                }
            }

            if (imgDstWidth < 3) {
                imgDstWidth = 3;
            }

            if (imgDstHeight < 3) {
                imgDstHeight = 3;
            }
        }

        return new Dimension(imgDstWidth, imgDstHeight);
    }

    /**
     * @param imageDimension
     * @param targetFormat
     * @return
     * @throws Exception
     */
    public static boolean shouldUseReferenceFormat(@NonNull final Dimension imageDimension, @NonNull final ImageFormat targetFormat) {
        // reference formats are possible for all different scaling types;
        // autoRotate=true and shrinkOnly=true flags are mandatory in order to use reference formats
        return ((targetFormat.isAutoRotate() == true) && targetFormat.isShrinkOnly() &&
            (compare(getResultingImageDimension(imageDimension, targetFormat), imageDimension) >= 0));
    }

    /**
     * @param first
     * @param second
     * @return
     */
    public static int compare(@NonNull final Dimension first, @NonNull final Dimension second) {
        // the used area of the given  dimensions is compared
        return Long.compare(((long) first.width) * first.height, ((long) second.width) * second.height);
    }

    /**
     * @param imageKey
     * @param imageFormatStr
     * @return
     * @throws ObjectCacheException
     * @throws IOException
     */
    public static InputStream readImageByTargetFormat(
        @NonNull IObjectCache objectCache,
        @NonNull final String imageKey,
        @NonNull final ImageFormat imageFormat,
        final boolean removeImageKeyOnError) throws ImageConverterException {

        final String imageFileName = imageFormat.getFormatString();

        try (final IReadAccess fileReadAcess = objectCache.getReadAccess(IMAGECONVERTER_GROUPID, imageKey, imageFileName)) {
            if (null != fileReadAcess) {
                final String referenceFormatStr = fileReadAcess.getKeyValue(ImageConverterUtils.IMAGECONVERTER_KEY_REFERENCE);

                if (StringUtils.isNotEmpty(referenceFormatStr)) {
                    return readImageByTargetFormat(objectCache, imageKey, ImageFormat.parseImageFormat(referenceFormatStr), removeImageKeyOnError);
                } else {
                    try (final InputStream resultInputStm = fileReadAcess.getInputStream()) {
                        if (null != resultInputStm) {
                            return new ByteArrayInputStream(IOUtils.toByteArray(resultInputStm));
                        }
                    }
                }
            }
        } catch (@SuppressWarnings("unused") Exception e) {
            if (removeImageKeyOnError) {
                LOG.warn(IC_STR_BUILDER().
                    append("error when reading image => Removing image key ").
                    append(IMAGECONVERTER_GROUPID).append('/').append(imageKey).toString());

                try {
                    objectCache.removeKey(IMAGECONVERTER_GROUPID, imageKey);
                } catch (Exception e1) {
                    LOG.error(Throwables.getRootCause(e1).getMessage());
                }
            }

            throw new ImageConverterException(e);
        }

        return null;
    }


    /**
     * @param imageKey
     * @return
     * @throws ImageConverterException
     */
    public static IMetadata readMetadata(
        @NonNull IObjectCache objectCache,
        @NonNull IMetadataReader metadataReader,
        @NonNull final String imageKey,
        final boolean removeImageKeyOnError) throws ImageConverterException {

        try {
            final ICacheObject metadataFileItem = objectCache.get(IMAGECONVERTER_GROUPID, imageKey, ImageConverterUtils.IMAGECONVERTER_METADATA_FILEID);

            // first check, if a metadata file item exists for the given key at all
            if (null != metadataFileItem) {
                // retrieve the metadata content for the given image key
                try (final IReadAccess fileReadAcess = objectCache.getReadAccess(metadataFileItem)) {
                    if (null != fileReadAcess) {
                        try (final InputStream resultInputStm = fileReadAcess.getInputStream()) {
                            if (null != resultInputStm) {
                                final BufferedReader reader = new BufferedReader(new InputStreamReader(resultInputStm, "UTF-8"));
                                StringBuilder jsonStrBuilder = STR_BUILDER();
                                String readStr = null;

                                while (null != (readStr = reader.readLine())) {
                                    jsonStrBuilder.append(readStr);
                                }

                                if (jsonStrBuilder.length() > 0) {
                                    return metadataReader.readMetadata(new JSONObject(jsonStrBuilder.toString()));
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            if (removeImageKeyOnError) {
                LOG.warn(IC_STR_BUILDER().
                    append("error when reading image metadata => Removing image key ").
                    append(IMAGECONVERTER_GROUPID).append('/').append(imageKey).toString());

                try {
                    objectCache.removeKey(IMAGECONVERTER_GROUPID, imageKey);
                } catch (Exception e1) {
                    LOG.error(Throwables.getRootCause(e1).getMessage());
                }
            }

            throw new ImageConverterException(e);
        }

        try {
            return metadataReader.readMetadata(new JSONObject());
        } catch (Exception e) {
            throw new ImageConverterException(e);
        }
    }

    /**
     * @param closeable
     */
    public static void close(final Closeable closeable) {
        if (null != closeable) {
            try {
                closeable.close();
            } catch (IOException e) {
                LOG.error(Throwables.getRootCause(e).getMessage());
            }
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * Moves an entry from the given imageFormatList to the returned candidate
     * list, if either the testSet is empty or the to be tested parameters of
     * the given testImageFormat, defined within the given test set, match the
     * parameters of one of the given imageFormatList entries
     *
     * @param imageFormatList
     * @param testParamSet
     * @return
     */
    final private static void implFillImageFormatCandidateList(@NonNull List<ImageFormat> workingImageFormatList, @NonNull final BitSet testParamSet, @NonNull final ImageFormat testImageFormat, Set<ImageFormat> candidateSet) {
        final boolean isAutoFormat = StringUtils.startsWithIgnoreCase(testImageFormat.getFormatShortName(), "auto");

        for (final Iterator<ImageFormat> iter = workingImageFormatList.iterator(); iter.hasNext(); ) {
            @NonNull final ImageFormat curImageFormat = iter.next();

            if (testParamSet.isEmpty() ||
                (!testParamSet.get(TEST_AUTOROTATE) || (curImageFormat.isAutoRotate() == testImageFormat.isAutoRotate())) &&
                (!testParamSet.get(TEST_SCALETYPE) || (curImageFormat.getScaleType().equals(testImageFormat.getScaleType())) &&
                (!testParamSet.get(TEST_SHRINKONLY) || (curImageFormat.isShrinkOnly() == testImageFormat.isShrinkOnly())) &&
                (!testParamSet.get(TEST_FORMATSHORTNAME) || isAutoFormat || curImageFormat.getFormatShortName().equals(testImageFormat.getFormatShortName())))) {

                // if all important attributes match, move format to candidate list
                candidateSet.add(curImageFormat);
                iter.remove();
            }
        }
    }

    // - Static members --------------------------------------------------------

    final private static TreeSet<ImageFormat> m_emptyCandidateSet = new TreeSet<>(new Comparator<ImageFormat>() {

        @Override
        public int compare(@NonNull ImageFormat imageFormat1, @NonNull ImageFormat imageFormat2) {
            return ImageConverterUtils.compare(
                new Dimension(imageFormat1.getWidth(), imageFormat1.getHeight()),
                new Dimension(imageFormat2.getWidth(), imageFormat2.getHeight()));
        }

    });

    final private static BitSet m_paramTestSet = new BitSet();

    final private static int TEST_AUTOROTATE = 0;
    final private static int TEST_SCALETYPE = 1;
    final private static int TEST_SHRINKONLY = 2;
    final private static int TEST_FORMATSHORTNAME = 3;

    final private static AtomicLong TEMP_COUNTER = new AtomicLong(0);

    static {
        // sort list of to be registered IMAGECONVERTER_KEYS custom keys
        Arrays.sort(IMAGECONVERTER_KEYS);

        m_paramTestSet.set(TEST_AUTOROTATE);
        m_paramTestSet.set(TEST_SCALETYPE);
        m_paramTestSet.set(TEST_SHRINKONLY);
        m_paramTestSet.set(TEST_FORMATSHORTNAME);
    }

}
