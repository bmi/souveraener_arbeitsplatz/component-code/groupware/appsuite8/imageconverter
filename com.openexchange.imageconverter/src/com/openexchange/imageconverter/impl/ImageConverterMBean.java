/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import static com.openexchange.imageconverter.impl.ImageConverterUtils.LOG;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import com.google.common.base.Throwables;
import com.openexchange.imageconverter.api.IImageConverterMonitoring;
import com.openexchange.imageconverter.api.ImageConverterPriority;
import com.openexchange.imageconverter.impl.ImageConverterMonitoring.RequestType;
import com.openexchange.management.AnnotatedStandardMBean;

public class ImageConverterMBean extends AnnotatedStandardMBean implements IImageConverterMonitoring {

    /**
     * Domain
     */
    final public static String DOMAIN = "com.openexchange.imageconverter";

    /**
     * Initializes a new {@link ImageConverterInformation}.
     * @param statistics Musn't be null
     * @throws NotCompliantMBeanException
     */
    public ImageConverterMBean(final ImageConverterMonitoring imageConverterMonitoring) throws NotCompliantMBeanException {
        super("MBean for ImageConverter", IImageConverterMonitoring.class);
        m_imageConverterMonitoring = imageConverterMonitoring;
    }

    /**
     * @return
     */
    public static ObjectName getObjectName() {
        try {
            return new ObjectName(DOMAIN, "name", "ImageConverterMonitoring");
        } catch (MalformedObjectNameException e) {
            LOG.error("IC exeption when creating ObjectName for MBean: {}", Throwables.getRootCause(e).getMessage());
        }

        return null;
    }

    // - IImageConverterMonitoring ---------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getCacheHitRatio()
     */
    @Override
    public double getCacheHitRatio() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getCacheHitRatio() : 0.0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getKeysProcessedCount()
     */
    @Override
    public long getKeysProcessedCount() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getKeysProcessedCount() : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getMedianKeyProcessTimeMilliss()
     */
    @Override
    public long getMedianKeyProcessTimeMillis() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianProcessTimeMillis() : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getKeyRequestCount_Total()
     */
    @Override
    public long getRequestCount_Total() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestCount(null) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getKeyRequestCount_Total()
     */
    @Override
    public long getRequestCount_Get() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestCount(RequestType.GET) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getKeyRequestCount_Background()
     */
    @Override
    public long getRequestCount_Cache() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestCount(RequestType.CACHE) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getKeyRequestCount_Medium()
     */
    @Override
    public long getRequestCount_CacheAndGet() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestCount(RequestType.CACHE_AND_GET) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getKeyRequestCount_Instant()
     */
    @Override
    public long getRequestCount_Admin() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestCount(RequestType.ADMIN) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getMedianKeyRequestTimeMillis_Total()
     */
    @Override
    public long getMedianRequestTimeMillis_Total() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianRequestTimeMillis(null) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getMedianKeyRequestTimeMillis_Total()
     */
    @Override
    public long getMedianRequestTimeMillis_Get() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianRequestTimeMillis(RequestType.GET) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getMedianKeyRequestTimeMillis_Background()
     */
    @Override
    public long getMedianRequestTimeMillis_Cache() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianRequestTimeMillis(RequestType.CACHE) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getMedianKeyRequestTimeMillis_Medium()
     */
    @Override
    public long getMedianRequestTimeMillis_CacheAndGet() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianRequestTimeMillis(RequestType.CACHE_AND_GET) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getMedianKeyRequestTimeMillis_Instant()
     */
    @Override
    public long getMedianRequestTimeMillis_Admin() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianRequestTimeMillis(RequestType.ADMIN) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getPeakKeyCountInQueue_Background()
     */
    @Override
    public long getPeakKeyCountInQueue_Background() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getPeakKeyCountInQueue(ImageConverterPriority.BACKGROUND) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getPeakKeyCountInQueue_Medium()
     */
    @Override
    public long getPeakKeyCountInQueue_Medium() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getPeakKeyCountInQueue(ImageConverterPriority.MEDIUM) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getPeakKeyCountInQueue_Instant()
     */
    @Override
    public long getPeakKeyCountInQueue_Instant() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getPeakKeyCountInQueue(ImageConverterPriority.INSTANT) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getMedianKeyQueueTimeMillis_Total()
     */
    @Override
    public long getMedianKeyQueueTimeMillis_Total() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianKeyQueueTimeMillis(null) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getMedianKeyQueueTimeMillis_Background()
     */
    @Override
    public long getMedianKeyQueueTimeMillis_Background() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianKeyQueueTimeMillis(ImageConverterPriority.BACKGROUND) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getMedianKeyQueueTimeMillis_Medium()
     */
    @Override
    public long getMedianKeyQueueTimeMillis_Medium() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianKeyQueueTimeMillis(ImageConverterPriority.MEDIUM) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getMedianKeyQueueTimeMillis_Instant()
     */
    @Override
    public long getMedianKeyQueueTimeMillis_Instant() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianKeyQueueTimeMillis(ImageConverterPriority.INSTANT) : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getCacheKeyCount()
     */
    @Override
    public long getCacheKeyCount() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getCacheKeyCount() : 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverterMonitoring#getCacheSize()
     */
    @Override
    public long getCacheSize() {
        return (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getCacheSize() : 0;
    }

    // - Members ---------------------------------------------------------------

    private ImageConverterMonitoring m_imageConverterMonitoring;
}
