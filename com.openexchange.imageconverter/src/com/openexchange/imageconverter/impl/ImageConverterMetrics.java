/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import com.openexchange.imageconverter.api.ImageConverterPriority;
import com.openexchange.imageconverter.impl.ImageConverterMonitoring.RequestType;
import com.openexchange.metrics.micrometer.Micrometer;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

//=============================================================================
public class ImageConverterMetrics {

    private static final String GROUP = "imageconverter";
    private static final String NO_UNIT = null;
    private static final String UNIT_MS = "ms";
    private static final Tags TAGS_CACHE = Tags.of("type", "cache");
    private static final Tags TAGS_KEYS = Tags.of("type", "keys");
    private static final Tags TAGS_REQUEST = Tags.of("type", "requests");
    private static final Tags TAGS_REQUESTS_GET = Tags.of("request", "get");
    private static final Tags TAGS_REQUESTS_CACHE = Tags.of("request", "cache");
    private static final Tags TAGS_REQUESTS_CACHE_AND_GET = Tags.of("request", "cacheAndGet");
    private static final Tags TAGS_REQUESTS_ADMIN = Tags.of("request", "admin");
    private static final Tags TAGS_REQUESTS_ALL = Tags.of("request", "all");
    private static final Tags TAGS_QUEUE_ALL = Tags.of("queue", "all");
    private static final Tags TAGS_QUEUE_BACKGROUND = Tags.of("queue", "background");
    private static final Tags TAGS_QUEUE_MEDIUM = Tags.of("queue", "medium");
    private static final Tags TAGS_QUEUE_INSTANT = Tags.of("queue", "instant");

    private ImageConverterMonitoring m_imageConverterMonitoring;

    public ImageConverterMetrics(ImageConverterMonitoring imageConverterMonitoring) {
        m_imageConverterMonitoring = imageConverterMonitoring;
    }

    public void registerMetrics() {
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".cache.hit.ratio", TAGS_CACHE, "The image cache hit ratio of requests.", NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getCacheHitRatio() : 0.0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".cache.key.count", TAGS_CACHE, "The number of image cache keys.", NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getCacheKeyCount() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".cache.size", TAGS_CACHE, "The size of the image cache.", NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getCacheSize() : 0.0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".keys.processed.count", TAGS_KEYS, "The number of processed keys.", NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getKeysProcessedCount() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".keys.processed.median.time", TAGS_KEYS, "The median time for key processing.", UNIT_MS, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianProcessTimeMillis() : 0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.count.total", TAGS_REQUEST, "The total number of requests.", NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestCount(null) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.count", TAGS_REQUESTS_GET, "The number of requests.", NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestCount(RequestType.GET) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.count", TAGS_REQUESTS_CACHE, null, NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestCount(RequestType.CACHE) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.count", TAGS_REQUESTS_CACHE_AND_GET, null, NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestCount(RequestType.CACHE_AND_GET) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.count", TAGS_REQUESTS_ADMIN, null, NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestCount(RequestType.ADMIN) : 0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.limitreached.count.total", TAGS_REQUEST, "The total number of requests exceeding the maximum request count.", NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestLimitReachedCount(null) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.limitreached.count", TAGS_REQUESTS_GET, "The number of requests exceeding the maximum request count.", NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestLimitReachedCount(RequestType.GET) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.limitreached.count", TAGS_REQUESTS_CACHE, null, NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestLimitReachedCount(RequestType.CACHE) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.limitreached.count", TAGS_REQUESTS_CACHE_AND_GET, null, NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestLimitReachedCount(RequestType.CACHE_AND_GET) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.limitreached.count", TAGS_REQUESTS_ADMIN, null, NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getRequestLimitReachedCount(RequestType.ADMIN) : 0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.median.time", TAGS_REQUESTS_ALL, "The median time of requests.", UNIT_MS, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianRequestTimeMillis(null) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.median.time", TAGS_REQUESTS_GET, null, UNIT_MS, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianRequestTimeMillis(RequestType.GET) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.median.time", TAGS_REQUESTS_CACHE, null, UNIT_MS, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianRequestTimeMillis(RequestType.CACHE) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.median.time", TAGS_REQUESTS_CACHE_AND_GET, null, UNIT_MS, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianRequestTimeMillis(RequestType.CACHE_AND_GET) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".request.median.time", TAGS_REQUESTS_ADMIN, null, UNIT_MS, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianRequestTimeMillis(RequestType.ADMIN) : 0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".queue.peak.count", TAGS_QUEUE_BACKGROUND, "The peak number of entries in the queue.", NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getPeakKeyCountInQueue(ImageConverterPriority.BACKGROUND) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".queue.peak.count", TAGS_QUEUE_MEDIUM, null, NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getPeakKeyCountInQueue(ImageConverterPriority.MEDIUM) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".queue.peak.count", TAGS_QUEUE_INSTANT, null, NO_UNIT, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getPeakKeyCountInQueue(ImageConverterPriority.INSTANT) : 0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".queue.median.time", TAGS_QUEUE_ALL, "The median waiting time of entries in the queue.", UNIT_MS, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianKeyQueueTimeMillis(null) : 0.0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".queue.median.time", TAGS_QUEUE_BACKGROUND, null, UNIT_MS, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianKeyQueueTimeMillis(ImageConverterPriority.BACKGROUND) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".queue.median.time", TAGS_QUEUE_MEDIUM, null, UNIT_MS, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianKeyQueueTimeMillis(ImageConverterPriority.MEDIUM) : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".queue.median.time", TAGS_QUEUE_INSTANT, null, UNIT_MS, this,
            (m) -> (null != m_imageConverterMonitoring) ? m_imageConverterMonitoring.getMedianKeyQueueTimeMillis(ImageConverterPriority.INSTANT) : 0);
    }
}
