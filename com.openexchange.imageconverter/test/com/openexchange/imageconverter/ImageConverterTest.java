/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter;

import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Properties;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.openexchange.context.ContextService;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.imageconverter.api.MetadataException;
import com.openexchange.imageconverter.impl.ImageConverterUtils;
import com.openexchange.imageconverter.impl.Services;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondService;

/**
 * Unit tests for {@link ImageConverterClient}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.4
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Services.class })
public class ImageConverterTest {

    /**
     * Mock of the {@link Session}
     */
    @Mock
    private Session m_session;

    /**
     * Mock of the {@link ContextService}
     */
    @Mock
    private ContextService m_contextService;

    /**
     * Mock of the {@link SessiondService}
     */
    @Mock
    private SessiondService m_sessiondService;

    /**
     * {@link Properties} mock
     */
    @Mock
    private Properties m_properties;

    /**
     * A temporary folder that could be used by each mock.
     */
    @Rule
    protected TemporaryFolder m_tmpFolder = new TemporaryFolder();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Services.class);

        // BEHAVIOUR
        PowerMockito.when(Integer.valueOf(m_session.getContextId())).thenReturn(Integer.valueOf(424242669));
        PowerMockito.when(m_session.getSessionID()).thenReturn("8a07c5a2e4974a75ae70bd9a36198f03");
    }

    @Test
    public void testImageFormat_ParseImageFormats() throws MetadataException {
        final String[] imageFormatStrings = {
            "nonsense",
            "JPG",
            "Pnf",
            "jpg-1",
            "jpg:+1",
            "png:-1x+1",
            "jpg:+2x+2",
            "jpg:-3x",
            "png:2x~",
            "png:2x2~@",
            "jpg:_2x@",
            "jpg:3x4~@74",
            "jpg:3x6@72",
            "jpg:3x4~cove",
            "jpg:3x4~cont76",
            "jpg:3x4~covcrop@",
            "jpg:3x4~cont@76",
            "png:3x4~contdimension@5",
            "png:3x4~contforcedimension@"
        };

        final StringBuilder imageFormatsBuilder = ImageConverterUtils.STR_BUILDER();

        for (final String curFormatStr : imageFormatStrings) {
            if (imageFormatsBuilder.length() > 0) {
                imageFormatsBuilder.append(';');
            }

            imageFormatsBuilder.append(curFormatStr);
        }

        final ImageFormat[] imageFormats = ImageFormat.parseImageFormats(imageFormatsBuilder.toString());

        for (int i = 0; i < imageFormatStrings.length; ++i) {
            System.out.println(imageFormatStrings[i] + ": " + imageFormats[i]);

        }

        assert(isNotEmpty(imageFormats));
    }

/*
    @Test (expected = MetadataException.class)
    public void testReadMetadata_FileNull_ThrowsMetadataException() throws MetadataException {
        final IMetadataReader reader = Services.getService(IMetadataReader.class);

        Assert.assertNotNull(reader);

        reader.readMetadata((File) null);
    }

    @Test (expected = MetadataException.class)
    public void testReadMetadata_InputStreamNull_ThrowsMetadataException() throws MetadataException {
        final IMetadataReader reader = Services.getService(IMetadataReader.class);

        Assert.assertNotNull(reader);

        reader.readMetadata((InputStream) null);
    }

    @Test (expected = MetadataException.class)
    public void testReadMetadata_JSONObjectNull_ThrowsMetadataException() throws MetadataException {
        final IMetadataReader reader = Services.getService(IMetadataReader.class);

        Assert.assertNotNull(reader);

        reader.readMetadata((JSONObject) null);
    }
*/

    // - Implementation --------------------------------------------------------

    /**
     * Writes chars from a <code>String</code> to bytes on an
     * <code>OutputStream</code> using the specified character encoding.
     */
    private static void write(String data, OutputStream outputStm, Charset encoding) throws IOException {
        if (null != data) {
            if (null == encoding) {
                IOUtils.write(data, outputStm);
            } else {
                outputStm.write(data.getBytes(encoding));
            }
        }
    }

    /**
     * Opens a {@link FileOutputStream} for the specified file, checking and
     * creating the parent directory if it does not exist.
     * <p>
     * At the end of the method either the stream will be successfully opened,
     * or an exception will have been thrown.
     * <p>
     * The parent directory will be created if it does not exist.
     * The file will be created if it does not exist.
     * An exception is thrown if the file object exists but is a directory.
     * An exception is thrown if the file exists but cannot be written to.
     * An exception is thrown if the parent directory cannot be created.
     *
     * @param append
     */
    private static FileOutputStream openOutputStream(File file, boolean append) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
                throw new IOException("File '" + file + "' exists but is a directory");
            }

            if (!file.canWrite()) {
                throw new IOException("File '" + file + "' cannot be written to");
            }
        } else {
            final File parent = file.getParentFile();

            if ((null != parent) && !parent.exists() && !parent.mkdirs()) {
                throw new IOException("File '" + file + "' could not be created");
            }
        }

        return new FileOutputStream(file, append);
    }
}

/*
    @Test
    public void testAcquireToken_tokenNull_ReturnNewToken() throws OXException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);

        String localToken = m_imageConverterClient.acquireToken(this.session);

        Mockito.verify(this.session, Mockito.times(1)).getSessionID();
        Assert.assertNotNull(localToken);
    }

    @Test
    public void testAcquireToken_FindToken_ReturnToken() throws OXException {
        this.m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);

        MockUtils.injectValueIntoPrivateField(m_imageConverterClient, "sessionId2token", createSessionId2Token());

        String localToken = m_imageConverterClient.acquireToken(this.session);

        Mockito.verify(this.session, Mockito.times(1)).getSessionID();
        Assert.assertNotNull(localToken);
    }

    @Test(expected = OXException.class)
    public void testRedeemToken_secretIsEmptyOrNotInMap_ThrowsOXException() throws OXException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService) {

            @Override
            public TokenLoginSecret getTokenLoginSecret(String secret) {
                return null;
            }
        };

        m_imageConverterClient.redeemToken(this.token, "appSecret", "optClientId", "optAuthId", "optHash", "optClientIp");
    }

    @Test(expected = OXException.class)
    public void testRedeemToken_SessiondServiceNull_ThrowsOXException() throws OXException {
        PowerMockito.when(Services.getService(SessiondService.class)).thenReturn(null);

        m_imageConverterClient = new ImageConverterClient(maxIdleTime, configService) {

            @Override
            public TokenLoginSecret getTokenLoginSecret(String secret) {
                return new DefaultTokenLoginSecret();
            }
        };

        m_imageConverterClient.redeemToken(this.token, "appSecret", "optClientId", "optAuthId", "optHash", "optClientIp");
    }

    @Test(expected = OXException.class)
    public void testRedeemToken_ContextServiceNull_ThrowsOXException() throws OXException {
        PowerMockito.when(Services.getService(SessiondService.class)).thenReturn(this.sessiondService);
        PowerMockito.when(Services.getService(ContextService.class)).thenReturn(null);

        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService) {

            @Override
            public TokenLoginSecret getTokenLoginSecret(String secret) {
                return new DefaultTokenLoginSecret();
            }
        };

        m_imageConverterClient.redeemToken(this.token, "appSecret", "optClientId", "optAuthId", "optHash", "optClientIp");
    }

    @Test(expected = OXException.class)
    public void testRedeemToken_NoSessionIdAvailableAndNoSuchToken_ThrowsOXException() throws OXException {
        PowerMockito.when(Services.getService(SessiondService.class)).thenReturn(this.sessiondService);
        PowerMockito.when(Services.getService(ContextService.class)).thenReturn(this.contextService);

        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService) {

            @Override
            public TokenLoginSecret getTokenLoginSecret(String secret) {
                return new DefaultTokenLoginSecret();
            }
        };

        m_imageConverterClient.redeemToken(this.token, "appSecret", "optClientId", "optAuthId", "optHash", "optClientIp");
    }

    @Test(expected = OXException.class)
    public void testRedeemToken_NotAbleToCreateNewSession_ThrowsOXException() throws OXException {
        PowerMockito.when(Services.getService(SessiondService.class)).thenReturn(this.sessiondService);
        PowerMockito.when(Services.getService(ContextService.class)).thenReturn(this.contextService);

        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService) {

            @Override
            public TokenLoginSecret getTokenLoginSecret(String secret) {
                return new DefaultTokenLoginSecret();
            }
        };

        MockUtils.injectValueIntoPrivateField(m_imageConverterClient, "token2sessionId", createToken2SessionId());

        m_imageConverterClient.redeemToken(this.token, "appSecret", "optClientId", "optAuthId", "optHash", "optClientIp");
    }

    @Test
    public void testRedeemToken_EverythingFine_ReturnSession() throws OXException {
        PowerMockito.when(this.sessiondService.getSession(Matchers.anyString())).thenReturn(this.session);
        PowerMockito.when(this.sessiondService.addSession((AddSessionParameter) Mockito.anyObject())).thenReturn(this.session);
        PowerMockito.when(Services.getService(SessiondService.class)).thenReturn(this.sessiondService);
        PowerMockito.when(Services.getService(ContextService.class)).thenReturn(this.contextService);

        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService) {

            @Override
            public TokenLoginSecret getTokenLoginSecret(String secret) {
                return new DefaultTokenLoginSecret();
            }
        };

        MockUtils.injectValueIntoPrivateField(m_imageConverterClient, "token2sessionId", createToken2SessionId());

        Session returnedSession = m_imageConverterClient.redeemToken(this.token, "appSecret", "optClientId", "optAuthId", "optHash", "optClientIp");

        Assert.assertNotNull(returnedSession);
        Mockito.verify(sessiondService, Mockito.times(1)).addSession((AddSessionParameter) Mockito.anyObject());
    }

    @Test
    public void testInitSecrets_SecretFileNull_ReturnEmptyMap() throws OXException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);

        Map<String, TokenLoginSecret> secrets = m_imageConverterClient.initSecrets(null);

        Assert.assertNotNull(secrets);
        Assert.assertEquals(0, secrets.size());
    }

    @Test
    public void testInitSecrets_CorrectInput_ReturnCorrectSize() throws OXException, IOException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);

        File file = folder.newFile("tokenlogin-secrets");
        writeStringToFile(file, secretFileContent, Charset.defaultCharset(), true);

        Map<String, TokenLoginSecret> initSecrets = m_imageConverterClient.initSecrets(file);

        Assert.assertEquals(1, initSecrets.size());
    }

    @Test
    public void testInitSecrets_CorrectInput_ReturnCorrectContent() throws OXException, IOException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);

        File file = folder.newFile("tokenlogin-secrets");
        writeStringToFile(file, secretFileContent, Charset.defaultCharset(), true);

        Map<String, TokenLoginSecret> initSecrets = m_imageConverterClient.initSecrets(file);

        Assert.assertNotNull(initSecrets.get(SECRET_TOKEN));
        Assert.assertEquals(SECRET_TOKEN, initSecrets.get(SECRET_TOKEN).getSecret());
    }

    @Test
    public void testInitSecrets_CorrectInput_ReturnAccessPasswordTrue() throws OXException, IOException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);

        File file = folder.newFile("tokenlogin-secrets");
        writeStringToFile(file, secretFileContent, Charset.defaultCharset(), true);

        Map<String, TokenLoginSecret> initSecrets = m_imageConverterClient.initSecrets(file);

        Assert.assertEquals(true, initSecrets.get(SECRET_TOKEN).getParameters().get("accessPassword"));
    }

    @Test
    public void testInitSecrets_TextInput_ReturnTextInputPasswordFalse() throws OXException, IOException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);

        String secret = "this should not be returned as correct";
        File file = folder.newFile("tokenlogin-secrets");
        writeStringToFile(file, secret, Charset.defaultCharset(), true);

        Map<String, TokenLoginSecret> initSecrets = m_imageConverterClient.initSecrets(file);

        Assert.assertEquals(1, initSecrets.size());
        Assert.assertEquals(secret, initSecrets.get(secret).getSecret());
    }

    @Test
    public void testInitSecrets_TextInput_ReturnTextInputPasswordTrue() throws OXException, IOException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);

        String secret = "this should not be returned as correct";
        String password = "; accessPassword=true";
        File file = folder.newFile("tokenlogin-secrets");
        writeStringToFile(file, secret + password, Charset.defaultCharset(), true);

        Map<String, TokenLoginSecret> initSecrets = m_imageConverterClient.initSecrets(file);

        Assert.assertEquals(true, initSecrets.get(secret).getParameters().get("accessPassword"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveTokenFor_SessionNull_ThrowException() throws OXException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);

        m_imageConverterClient.removeTokenFor(null);
    }

    @Test
    public void testRemoveTokenFor_TokenNotAvailable_TokenStillInMap() throws OXException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);
        MockUtils.injectValueIntoPrivateField(m_imageConverterClient, "token2sessionId", createToken2SessionId());

        m_imageConverterClient.removeTokenFor(this.session);

        Mockito.verify(this.session, Mockito.times(1)).getSessionID();
        Cache<String, String> token2sessionMap = (Cache<String, String>) MockUtils.getValueFromField(m_imageConverterClient, "token2sessionId");
        Assert.assertNotNull(token2sessionMap);
        Assert.assertEquals(1, token2sessionMap.size());
    }

    @Test
    public void testRemoveTokenFor_TokenNotAvailadfdble_Return() throws OXException {
        m_imageConverterClient = new ImageConverterClient(this.maxIdleTime, this.configService);
        MockUtils.injectValueIntoPrivateField(m_imageConverterClient, "token2sessionId", createToken2SessionId());
        MockUtils.injectValueIntoPrivateField(m_imageConverterClient, "sessionId2token", createSessionId2Token());

        m_imageConverterClient.removeTokenFor(this.session);

        Cache<String, String> token2sessionMap = (Cache<String, String>) MockUtils.getValueFromField(m_imageConverterClient, "token2sessionId");
        Assert.assertNotNull(token2sessionMap);
        Assert.assertEquals(0, token2sessionMap.size());
    }

    private Cache<String, String> buildCache() {
        CacheBuilder<Object, Object> builder = CacheBuilder.newBuilder().concurrencyLevel(4).maximumSize(Integer.MAX_VALUE).initialCapacity(1024).expireAfterAccess(maxIdleTime, TimeUnit.MILLISECONDS);
        Cache<String, String> cache = builder.build();
        return cache;
    }
}
*/