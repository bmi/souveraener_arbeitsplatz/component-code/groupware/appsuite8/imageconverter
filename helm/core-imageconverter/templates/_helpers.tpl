{{/*
Create ic-configmap name.
*/}}
{{- define "core-imageconverter.configmap" -}}
{{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "ic-configmap" }}
{{- end -}}

{{/*
Create spoolDir name.
*/}}
{{- define "core-imageconverter.spoolDir" -}}
{{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "spooldir" }}
{{- end -}}

{{/*
Create errorDir name.
*/}}
{{- define "core-imageconverter.errorDir" -}}
  {{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "errordir" }}
{{- end -}}

{{/*
Create fileItemStore name.
*/}}
{{- define "core-imageconverter.fileItemStore" -}}
  {{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "fileitemstore" }}
{{- end -}}

{{/*
ConfigMap
*/}}
{{- define "core-imageconverter.imageConverterConfigMap" -}}
{{- printf "%s-%s" .Release.Name "core-imageconverter" | trunc 63 | trimSuffix "-" }}
{{- end -}}

{{/*
Definition of ObjectStoreIds for the ObjectCache
*/}}
{{- define "core-imageconverter.objectCache.objectStoreId" -}}
{{- if .Values.objectCache.fileStore.id }}
value: {{ .Values.objectCache.fileStore.id }}
{{- else if and (not .Values.objectCache.fileStores) (not .Values.objectCache.s3ObjectStores) (not .Values.objectCache.sproxydObjectStores) }}
valueFrom:
  configMapKeyRef:
      name: {{ include "core-imageconverter.imageConverterConfigMap" . }}
      key: filestore
{{- else }}
value:
{{- end }}
{{- end -}}

{{/*
Create envVars secret.
*/}}
{{- define "core-imageconverter.envVars" -}}
{{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "envvars" }}
{{- end -}}
