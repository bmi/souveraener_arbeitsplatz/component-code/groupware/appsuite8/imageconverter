{{- define "core-imageconverter.ingressPathMappings" -}}
paths:
  - pathType: Prefix
    path: /imageconverter
    targetPath: /imageconverter/
    targetPort:
      name: http
  - pathType: Exact
    path: /imageconverter-metrics
    targetPath: /metrics
    targetPort:
      name: http
  - pathType: Prefix
    path: /objectcache
    targetPath: /objectcache/
    targetPort:
      name: http
  - pathType: Exact
    path: /objectcache-metrics
    targetPath: /metrics
    targetPort:
      name: http
{{- end -}}
