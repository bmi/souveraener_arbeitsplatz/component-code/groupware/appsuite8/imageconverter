/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.client;

import static com.openexchange.imageconverter.client.ImageConverterClient.LOG;
import static org.apache.commons.lang3.StringUtils.indexOfIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.lastIndexOfIgnoreCase;
import java.io.BufferedReader;
import java.io.StringReader;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.google.common.base.Throwables;
import com.openexchange.annotation.NonNull;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.imageconverter.api.ImageServerUtil;
import com.openexchange.imageconverter.client.generated.modules.ImageConverterApi;

/**
 * {@link ImageConverterRemoteValidator}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
class ImageConverterRemoteValidator {

    /**
     * ESTABLISH_CONNECTION_DELAY_MILLIS
     */
    final protected static int ESTABLISH_CONNECTION_DELAY_MILLIS = 10000;

    /**
     * ESTABLISH_CONNECTION_PERIOD_MILLIS
     */
    final protected static int ESTABLISH_CONNECTION_PERIOD_MILLIS = 3000;

    /**
     * ESTABLISH_CONNECTION_TIMEOUT_MILLIS
     */
    final protected static int ESTABLISH_CONNECTION_TIMEOUT_MILLIS = 10000;

    /**
     * CHECK_REMOTE_CONNECTION_TIMEOUT_MILLIS
     */
    final protected static int CHECK_REMOTE_CONNECTION_TIMEOUT_MILLIS = 20000;

    /**
     * ENABLE_LOG_PERIOD_MILLIS
     */
    final protected static long ENABLE_LOG_PERIOD_MILLIS = 60000;

    /**
     * {@link ImageConverterServerValidatorCallable}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.0
     */
    protected class ImageConverterServerValidatorCallable implements Callable<Integer> {

        /**
         * Initializes a new {@link ImageConverterServerValidatorCallable}.
         */
        protected ImageConverterServerValidatorCallable() {
            super();
        }

        /**
         * @return
         * @throws Exception
         */
        @Override
        public Integer call() throws Exception {
            final ImageConverterApi imageConverterApi = m_imageConverterApiProvider.acquire();
            final int oldRemoteApiVersion = m_remoteAPIVersion.get();
            int remoteApiVersion = 0;
            String statusResponse = null;

            if (null != imageConverterApi) {
                try {
                    if (isNotEmpty(statusResponse = imageConverterApi.status("false"))) {
                        statusResponse = statusResponse.toLowerCase();

                        try (final BufferedReader stringReader = new BufferedReader(new StringReader(statusResponse))) {
                            for (String readLine = null; (readLine = stringReader.readLine()) != null;) {
                                int indexOf = 0;

                                if ((remoteApiVersion <= 0) &&
                                    ((indexOf = indexOfIgnoreCase(readLine, "id:")) > -1) &&
                                    ((indexOf = indexOfIgnoreCase(readLine.substring(indexOf + 3), ImageServerUtil.IMAGE_SERVER_ID)) > -1)) {

                                    remoteApiVersion = 1;
                                }

                                if ((indexOf = lastIndexOfIgnoreCase(readLine, "api:")) > -1) {
                                    final Matcher matcher = API_VERSION_PATTERN.matcher(readLine.substring(indexOf));

                                    if (matcher.find()) {
                                        remoteApiVersion = Integer.valueOf(matcher.group(1)).intValue();
                                    }
                                }
                            }
                       }
                    }
                } catch (Throwable e) {
                    ExceptionUtils.handleThrowable(e);
                    LOG.trace("IC client RemoteValidator not able to get remote status of server", e);
                } finally {
                    // disconnect and try again, if no valid answer has been received so far
                    if (!implIsValidAPIVersion(remoteApiVersion)) {
                        if (implIsValidAPIVersion(oldRemoteApiVersion)) {
                            m_remoteAPIVersion.set(-1);
                        } else {
                            final long curLogTime = System.currentTimeMillis();

                            if ((0 == m_lastLogTime) || ((curLogTime - m_lastLogTime) >= ENABLE_LOG_PERIOD_MILLIS)) {
                                LOG.error("IC client remote connection could not be established => please check remote ImageConverter server setup at: {}", m_remoteUrl);
                                m_lastLogTime = curLogTime;
                            }
                        }
                    }

                    m_imageConverterApiProvider.release(imageConverterApi);
                }
            }

            if (!implIsValidAPIVersion(oldRemoteApiVersion) && implIsValidAPIVersion(remoteApiVersion)) {
                LOG.info("IC client established remote connection to ImageConverter server at: {}", m_remoteUrl);
            }

            m_remoteAPIVersion.set(remoteApiVersion);
            m_validationRunning.compareAndSet(true, false);

            return Integer.valueOf(remoteApiVersion);
        }
    }

    /**
     * @param remoteUrl
     */
    ImageConverterRemoteValidator(@NonNull final ImageConverterClientConfig clientConfig, @NonNull final ImageConverterApiProvider apiProvider) {
        super();

        m_imageConverterClientConfig = clientConfig;
        m_imageConverterApiProvider  = apiProvider;
        m_callable = new ImageConverterServerValidatorCallable();
        m_remoteUrl = m_imageConverterClientConfig.getImageConverterServerURL();

        if (implIsValidRemoteUrl()) {
            // initial trigger at end of construction
            m_executorService.submit(() -> {
                Thread.sleep(Math.max(0, ESTABLISH_CONNECTION_DELAY_MILLIS));
                return Boolean.valueOf(getRemoteAPIVersion(false) >= 1);
            });
        }
    }

    /**
     * @return
     */
    boolean isConnected(boolean force) {
        return (getRemoteAPIVersion(force) >= 1);
    }


    /**
     * @return
     */
    synchronized int getRemoteAPIVersion(final boolean force) {
        final int oldRemoteAPIVersion = m_remoteAPIVersion.get();

        if (implIsValidRemoteUrl() && (force || !implIsValidAPIVersion(oldRemoteAPIVersion))) {
            final Future<Integer> resultFuture = m_executorService.submit(m_callable);

            if (null != resultFuture) {
                try {
                    return resultFuture.get().intValue();
                } catch (Exception e) {
                    LOG.error("IC client remote validator caught exception: {}", Throwables.getRootCause(e).getMessage());
                }
            }
        }

        return oldRemoteAPIVersion;
    }

    /**
     * updateRemoteURL
     *
     * @param remoteUrl
     */
    synchronized void updateRemoteURL(final URL remoteUrl) {
        if ((null != remoteUrl) && (!implIsValidRemoteUrl() || !m_remoteUrl.equals(remoteUrl))) {

            m_remoteAPIVersion.set(-1);
            m_validationRunning.set(false);
            m_remoteUrl = (null != remoteUrl) && (remoteUrl.toString().length() > 0) ? remoteUrl : null;

            // trigger new connection validation and get current API version
            final int newConnectionAPIVersion = getRemoteAPIVersion(true);

            if (implIsValidAPIVersion(newConnectionAPIVersion)) {
                LOG.info("IC client successfully connected to remote server after configuration update.");
            } else {
                LOG.error("IC client could not connect to remote server after configuration update!");
            }
        }
    }

    /**
     *
     */
    synchronized void connectionLost() {
        if (implIsValidRemoteUrl() && implIsValidAPIVersion(m_remoteAPIVersion.get())) {
            m_remoteAPIVersion.set(-1);
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * implIsValidRemoteUrl
     *
     * @return
     */
    private boolean implIsValidRemoteUrl() {
        return (null != m_remoteUrl);
    }

    /**
     * implIsValidAPIVersion
     *
     * @param apiVersion
     * @return
     */
    private static boolean implIsValidAPIVersion(final int apiVersion) {
        return (apiVersion > 0);
    }

    final protected static Pattern API_VERSION_PATTERN = Pattern.compile("[aA][pP][iI]\\:\\s*v([0-9]+)");

    // - Members ---------------------------------------------------------------

    final protected ImageConverterRemoteValidator self = this;

    final protected ExecutorService m_executorService = Executors.newCachedThreadPool();

    final protected AtomicBoolean m_validationRunning = new AtomicBoolean(false);

    final protected AtomicInteger m_remoteAPIVersion = new AtomicInteger(-1);

    final protected ImageConverterClientConfig m_imageConverterClientConfig;

    final protected ImageConverterApiProvider m_imageConverterApiProvider;

    final protected ImageConverterServerValidatorCallable m_callable;

    protected URL m_remoteUrl;

    protected long m_lastLogTime = 0;
}
