/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.client.osgi;

import org.eclipse.microprofile.health.HealthCheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Throwables;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Reloadable;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.imageconverter.api.IImageClient;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.client.ImageConverterClient;
import com.openexchange.imageconverter.client.ImageConverterClientHealthCheck;
import com.openexchange.imageconverter.client.Services;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link ImageConverterClientActivator}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageConverterClientActivator extends HousekeepingActivator {

    /**
     * Initializes a new {@link ImageConverterClientActivator}.
     */
    public ImageConverterClientActivator() {
        super();
    }

    //-------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] {
            ConfigurationService.class,
            SSLSocketFactoryProvider.class
        };
    }

    @Override
    protected Class<?>[] getOptionalServices() {
        return new Class<?>[] {
            IMetadataReader.class
        };
    }

    //-------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    public void startBundle() throws Exception {
        LOG.info("starting bundle: {}", SERVICE_NAME);

        Services.setServiceLookup(this);

        try {
            // configuration
            final ConfigurationService configService = getService(ConfigurationService.class);

            if (null == configService) {
                throw new OXException(new Throwable("Configuration service not available"));
            }

            // try to get configured SSLSocketfactory
            final SSLSocketFactoryProvider sslSocketFactoryProvider = getService(SSLSocketFactoryProvider.class);

            if (null == sslSocketFactoryProvider) {
                throw new OXException(new Throwable("SSLSocketFactoryProvider service not available"));
            }

            // register IImageClient service
            registerService(IImageClient.class, m_imageConverterClient = new ImageConverterClient(
                configService,
                sslSocketFactoryProvider.getOriginatingDefaultContext(),
                sslSocketFactoryProvider.getDefault()));

            // register IImageClient service as Reloadable configuration
            // service for changing the remote URL config item at runtime
            registerService(Reloadable.class, m_imageConverterClient);
            registerService(HealthCheck.class, new ImageConverterClientHealthCheck(m_imageConverterClient));

            openTrackers();

            LOG.info("Successfully started bundle: {}", SERVICE_NAME);
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            LOG.error("... starting bundle {} failed: {}", SERVICE_NAME, Throwables.getRootCause(e).getMessage());
            throw new RuntimeException(e);
        }
    }

    //-------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    public void stopBundle() throws Exception {
        LOG.info("stopping bundle: {}", SERVICE_NAME);

        closeTrackers();
        unregisterServices();
        Services.setServiceLookup(null);

        m_imageConverterClient.close();
        m_imageConverterClient = null;

        LOG.info("successfully stopped bundle: {}", SERVICE_NAME);
    }

    // - Members ---------------------------------------------------------------

    protected ImageConverterClient m_imageConverterClient = null;

    // - Static Members --------------------------------------------------------

    final private static String SERVICE_NAME = "Open-Xchange ImageConverter Client";

    final private static Logger LOG = LoggerFactory.getLogger(ImageConverterClientActivator.class);
}
